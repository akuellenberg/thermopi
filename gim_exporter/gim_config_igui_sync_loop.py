import logging
import tornado
import json
import signal
from tornado.gen import coroutine, sleep
from argparse import ArgumentParser
from katcp import KATCPClientResource
from igui import Node, IGUI

log = logging.getLogger("sync-loop")


@coroutine
def update(rc, igui, root, previous_config, polling=5.0):
    log.debug("Syncing with KATCP client")
    yield rc.until_synced()
    log.debug("Synced")
    log.info("Fetching gim-config")
    response = yield rc.req.gim_config(timeout=10)
    if response.reply.reply_ok():
        config = json.loads(response.reply.arguments[1].decode())
        log.debug(config)
    else:
        log.exception("Error on ?gim-config call to server: {}".format(
            response.reply.arguments[1]))
    log.info("gim-config retrieved successfully")
    if config == previous_config:
        log.info("No updates, nothing to be done")
    else:
        log.info("Updating and syncing node tree")
        root.update_from_config(config)
        root.sync_with_igui(igui, update=True)
    yield sleep(polling)
    tornado.ioloop.IOLoop.current().add_callback(update, rc, igui, root, config, polling)


@coroutine
def on_shutdown(ioloop, client):
    log.info("Shutting down client")
    client.stop()
    ioloop.stop()


def main():
    usage = "usage: %prog [options]"
    parser = ArgumentParser(description=usage)
    parser.add_argument('--host', action='store', dest='host', type=str,
                        help='The hostname for the KATCP server to connect to')
    parser.add_argument(
        '--port', action='store', dest='port', type=int,
        help='The port number for the KATCP server to connect to')
    parser.add_argument('--igui-url', action='store', dest='igui_url',
                        type=str, help='The IGUI URL address to connect to',
                        default="http://localhost:30001")
    parser.add_argument('--igui-user', action='store', dest='igui_user',
                        type=str, help='The IGUI username', default="admin")
    parser.add_argument('--igui-pass', action='store', dest='igui_pass',
                        type=str, help='The IGUI password', default="admin")
    parser.add_argument('--igui-parent', action='store', dest='igui_parent',
                        type=str, help='The IGUI parent ID')
    parser.add_argument('--polling-interval', action='store', dest='polling_interval',
                        type=int, help='Cycle time for update loop', default=5.0)

    parser.add_argument(
        '--log-level',
        action='store',
        dest='log_level',
        type=str,
        help='Logging level',
        default="INFO")
    args = parser.parse_args()
    FORMAT = "[ %(levelname)s - %(asctime)s - %(filename)s:%(lineno)s] %(message)s"
    logger = logging.getLogger('sync-loop')
    logging.basicConfig(format=FORMAT)
    logger.setLevel(args.log_level.upper())
    logging.getLogger('igui-api-python').setLevel(args.log_level.upper())
    logging.getLogger('katcp').setLevel('INFO')
    ioloop = tornado.ioloop.IOLoop.current()

    log.info("Creating KATCP client")
    rc = KATCPClientResource(dict(
        name="sidecar-client",
        address=(args.host, args.port),
        controlled=True))

    signal.signal(
        signal.SIGINT,
        lambda sig, frame: ioloop.add_callback_from_signal(
            on_shutdown, ioloop, rc))

    log.info("Creating IGUI client")
    igui = IGUI(args.igui_url, args.igui_user, args.igui_pass)
    log.info("Authenticating with IGUI")
    igui.authorize()

    log.info("Creating node tree")
    root = Node(args.igui_parent, Node.DEVICES, parent=None)

    def startup():
        rc.start()
        tornado.ioloop.IOLoop.current().add_callback(update, rc, igui, root, args.polling_interval)
        log.info("Ctrl-C to terminate client")

    ioloop.add_callback(startup)
    ioloop.start()


if __name__ == "__main__":
    main()