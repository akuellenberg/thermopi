import logging
import asyncio
import httpx
import time

log = logging.getLogger("gim-api-python")

KATCP_DELIMITER = "."
GIM_DELIMITER = "::"
DEFAULT_METADATA = {"mysql_task_type": "GET"}


class NodeDoesNotExist(Exception):
    pass


class RootNodeDoesNotExist(Exception):
    pass


class Reauthenticated(Exception):
    pass


class MaxRetriesExceeded(Exception):
    pass


def retry(retries=5, wait=1.0, fatal_exceptions=None):
    def make_wrapper(fn):
        async def wrapper(*args, **kwargs):
            nonlocal fatal_exceptions
            if not fatal_exceptions:
                fatal_exceptions = []
            attempts = 0
            response = None
            current_wait = wait
            while attempts <= retries:
                current_wait = min(current_wait, 10.0)
                log.debug(f"Calling {repr(fn)} with args ({args}, {kwargs}), attempt {attempts}")
                try:
                    response = await fn(*args, **kwargs)
                except fatal_exceptions as error:
                    log.exception(repr(error))
                    raise error
                except httpx.HTTPStatusError as error:
                    if error.response.status_code == 503:
                        log.warning(f"{repr(fn)} returned a 503 error, retrying with linear backoff")
                        await asyncio.sleep(current_wait)
                        current_wait += 1.0
                        continue
                    else:
                        log.exception(error)
                        await asyncio.sleep(current_wait)
                        attempts += 1
                        continue
                except Exception as error:
                    log.exception(error)
                    await asyncio.sleep(current_wait)
                    attempts += 1
                    continue
                else:
                    break
            else:
                raise MaxRetriesExceeded
            return response

        return wrapper

    return make_wrapper


def guarantee(fn):
    async def wrapper(*args, **kwargs):
        while True:
            try:
                response = await fn(*args, **kwargs)
            except Exception as error:
                log.exception(error)
                await asyncio.sleep(5)
                continue
            else:
                break
        return response

    return wrapper


class Node(object):
    DEVICES, TASKS = ["Devices", "Tasks"]

    def __init__(self, node_name, node_type, config=None, parent=None):
        self._children = {}
        self.node_name = node_name
        self.node_type = node_type
        self.config = config if config is not None else {}
        self.node_id = None
        self.description = None
        self.parent = parent
        self.last_sensor_reading = None
        self._sensor_map = None
        self._node_id_map = {}

    def __iter__(self):
        yield self
        for child in self._children.values():
            yield from child

    def update_from_config(self, config):
        log.debug("Updating {} configuration".format(self))
        for name, device in config.get("devices", {}).items():
            log.debug("Creating sub-configuration for device: {}".format(name))
            device_config = {key: value for key, value in device.items()
                             if key not in ["devices", "tasks"]}
            self.add_node(name, self.DEVICES, device_config)
            self._children[name].update_from_config(device)
        for name, task in config.get("tasks", {}).items():
            log.debug("Adding task: {}".format(name))
            name = name.split(".")[-1]
            self.add_node(name, self.TASKS, {key: value for key, value in task.items()})
        self._refresh_sensor_map()
        self.inject_sidecars()

    def inject_sidecars(self):
        def selector(device):
            if device.parent is None:
                return False
            else:
                return device.node_name != "sidecar"

        devices = self.list_devices(filter_func=selector)
        for device in devices:
            if "sidecar" not in device._children.keys():
                device.add_node(
                    "sidecar.connected", self.TASKS,
                    {"current_value": 0,
                     "timestamp": "{:.6f}".format(time.time()),
                     "error_state": 0,
                     "metadata": {
                         "upper_limit": 1,
                         "lower_limit": 0,
                         "mysql_task_type": "GET",
                         "value_type": "int",
                         "description": "Is sidecar connected to server"}
                     })

    def _lookup(self, sensor_name, delimiter):
        path = sensor_name.split(delimiter)
        base = self._children[path[0]]
        if len(path) == 1:
            return base
        else:
            return base._lookup(delimiter.join(path[1:]), delimiter)

    def _refresh_id_map(self):
        self._node_id_map = {}
        for node in self:
            if node.node_id:
                self._node_id_map[node.node_id] = node

    def _refresh_sensor_map(self):
        self._sensor_map = {}
        tasks = self.list_tasks(
            filter_func=lambda node:
            node.config.get("options", "") in ["get", "get_set"])
        for task in tasks:
            self._sensor_map[task.sensor_name()] = task

    def sensor_lookup(self, sensor_name, refresh=False):
        if self._sensor_map is None or refresh:
            self._refresh_sensor_map()
        return self._sensor_map[sensor_name]

    def task_lookup(self, task_name):
        return self._lookup(task_name, GIM_DELIMITER)

    def task_lookup_by_id(self, node_id):
        if self._node_id_map is None:
            self._refresh_id_map()
        try:
            return self._node_id_map[node_id]
        except KeyError:
            self._refresh_id_map()
            try:
                return self._node_id_map[node_id]
            except KeyError:
                raise NodeDoesNotExist

    def list_nodes(self, filter_func=None, node_type=None):
        nodes = []
        for node in self:
            if node_type is None or node.node_type == node_type:
                if filter_func is not None:
                    if filter_func(node):
                        nodes.append(node)
                else:
                    nodes.append(node)
        return nodes

    def list_tasks(self, filter_func=None):
        return self.list_nodes(filter_func, self.TASKS)

    def list_devices(self, filter_func=None):
        return self.list_nodes(filter_func, self.DEVICES)

    async def sync_with_gim(self, client, update=True):
        log.debug("Syncing Node tree with iGUI")

        def add_children(branch, base):
            out = {}
            if "data" not in branch or branch["data"] is None:
                return out
            new_base = GIM_DELIMITER.join([base, branch["data"]["node_name"]])
            out[new_base] = branch["id"]
            for child in branch["children"]:
                out.update(add_children(child, new_base))
            return out

        log.info("Fetching GIM tree for {}".format(self.task_name()))
        while True:
            try:
                tree = await client.get_tree(self.task_name())
            except httpx.HTTPStatusError as error:
                if error.response.status_code == 500:
                    log.warning("Server busy, waiting and retrying (500 response code)")
                    await asyncio.sleep(5)
            else:
                break
        log.debug("Retrieved GIM tree for {}".format(self.task_name()))
        id_map = add_children(tree, "")
        for node in self:
            key = node.task_name()
            if key in id_map:
                node.node_id = id_map[key]
        for node in self:
            await node._sync_node(client, update)
        log.debug("Sync complete")

    async def _sync_node(self, client, update=True):
        if not self.node_id:
            log.debug("No ID known for {}".format(self))
            try:
                data = await client.get_node(self.task_name())
                self.node_id = data["node_id"]
                log.debug("Node already exists with ID: {}".format(self.node_id))
            except NodeDoesNotExist:
                if not self.parent:
                    raise RootNodeDoesNotExist
                else:
                    await self.parent._sync_node(
                        client, update=update)
                log.debug("Creating node {} with parent {}".format(
                    self.node_name, self.parent.task_name()))
                self.node_id = await client.create_node(
                    self.node_name, self.parent.task_name(),
                    self.node_type, self.config.get("metadata", DEFAULT_METADATA))
        if update and self.parent and self.config:
            try:
                await client.update_node(
                    self.task_name(),
                    {"metadata": self.config.get("metadata", DEFAULT_METADATA)})
            except Exception as error:
                log.warning(repr(error))
        return self.node_id

    def get_config_parameter(self, key):
        if key in self.config:
            return self.config[key]
        elif key in self.config.get("metadata", {}):
            return self.config["metadata"][key]
        elif self.parent is not None:
            return self.parent.get_config_parameter(key)
        else:
            raise KeyError

    def request_name(self):
        if self.node_type == self.DEVICES:
            return None
        custom_mapping = self.config.get("request_name", None)
        if custom_mapping:
            log.debug(f"Custom request mapping: {custom_mapping}")
            return custom_mapping
        else:
            auto = self._autogen_request_name()
            log.debug(f"Auto request mapping: {auto}")
            return auto

    def _autogen_request_name(self):
        if self.parent is None:
            return ""
        prefix = self.parent._autogen_request_name()
        if prefix == "":
            return self.node_name
        else:
            return "{}_{}".format(prefix, self.node_name)

    def sensor_name(self):
        if self.node_type == self.DEVICES:
            return None
        custom_mapping = self.config.get("sensor_name", None)
        if custom_mapping:
            return custom_mapping
        else:
            return self._autogen_sensor_name()

    def _autogen_sensor_name(self):
        if self.parent is None:
            return ""
        prefix = self.parent._autogen_sensor_name()
        if prefix == "":
            return self.node_name
        else:
            return "{}.{}".format(prefix, self.node_name)

    def task_name(self):
        if self.parent is None:
            return self.node_name
        return "{}::{}".format(self.parent.task_name(), self.node_name)

    def add_node(self, path, node_type, config, delimiter=KATCP_DELIMITER):
        split_path = path.split(delimiter)
        if len(split_path) == 1:
            node = Node(split_path[0], node_type, config=config, parent=self)
            self._children[split_path[0]] = node
            return node
        else:
            if split_path[0] not in self._children:
                self._children[split_path[0]] = Node(split_path[0], self.DEVICES, parent=self)
            return self._children[split_path[0]].add_node(".".join(split_path[1:]), node_type, config)

    def __repr__(self):
        return "<Node: {} ({})>".format(self.task_name(), self.node_type)


DEFAULT_TIMEOUT = 100  # seconds


async def log_request(request):
    log.debug(f"Request event hook: {request.method} {request.url} - Waiting for response")


async def log_response(response):
    await response.aread()
    request = response.request
    log.debug(f"Response event hook: {request.method} {request.url} - Status {response.status_code}")
    log.debug(f"Response payload: {response.text}")


class GIM(object):
    AUTH_URL = "/icom/authorize"
    TREE_URL = "/icom/api/trees"
    LOG_URL = "/icom/api/logs"
    NODE_URL = "/icom/api/nodes"
    NODE_BULK_URL = "/icom/api/nodes/batch"
    NODE_QUERY_URL = "/icom/api/nodes/query"

    LOGIN_SUCCESS_RESPONSE = "true"

    TASK_COMPLETE = 0
    TASK_PENDING = 1
    TASK_RUNNING = 2
    TASK_FAILED = 3

    def __init__(self, url, user, password, max_connections=100, retries=3):
        self._gim_url = url
        self._user = user
        self._password = password

        limits = httpx.Limits(max_connections=max_connections)
        transport = httpx.AsyncHTTPTransport(retries=retries)
        event_hooks = {
            'request': [log_request],
            'response': [log_response, self.raise_on_4xx_5xx]
        }
        self._session = httpx.AsyncClient(
            base_url=self._gim_url,
            transport=transport,
            limits=limits,
            event_hooks=event_hooks,
            timeout=DEFAULT_TIMEOUT)

    def __del__(self):
        pass

    async def shutdown(self):
        await self._session.aclose()

    async def raise_on_4xx_5xx(self, response):
        try:
            response.raise_for_status()
        except httpx.HTTPStatusError as error:
            if error.response.status_code == 404:
                raise NodeDoesNotExist
            elif error.response.status_code == 401:
                log.warning("Received 401 error, reauthenticating")
                await self.authorize()
                raise Reauthenticated
            else:
                request = response.request
                log.error(
                    f"Response event hook: {request.method} {request.url} - Status {response.status_code} [{response.text}]")
                raise error

    @guarantee
    async def authorize(self):
        """
        @brief   Authorize access to GIM via user:password
        """
        log.info("Authenticating with GIM instance at {}".format(
            self.url_for(self.AUTH_URL)))
        response = await self._session.get(
            self.url_for(self.AUTH_URL),
            auth=(self._user, self._password))
        if response.text == self.LOGIN_SUCCESS_RESPONSE:
            log.info("Successfully authorized with GIM")
        else:
            raise Exception("Unknown exception on login (code={})".format(
                response.status_code))

    def url_for(self, path, idx=None, params=None):
        params_str = ""
        if params:
            params_str = "?" + "&".join(
                ["{}={}".format(key, value)
                 for key, value in params.items()])
        return '{}{}{}'.format(
            path,
            "/{}".format(idx) if idx else "",
            params_str)

    @guarantee
    async def create_node(self, name, parent, node_type, metadata=None):
        assert node_type in ["Devices", "Tasks"]
        full_path = "{}::{}".format(parent, name)
        log.info("Creating new node {} (full path = {}, type = {})".format(
            name, full_path, node_type))
        if not metadata:
            metadata = DEFAULT_METADATA
        log.info("POST {} {}".format(self.url_for(self.NODE_URL), {
            'node_type': node_type,
            'metadata': metadata,
            'node_name': name,
            'parent_name': parent}))
        response = await self._session.post(
            self.url_for(self.NODE_URL),
            json={
                'node_type': node_type,
                'metadata': metadata,
                'node_name': name,
                'parent_name': parent})
        log.info(response.text)
        node_id = response.json()["node_id"]
        log.info("Created new node '{}' with ID {}".format(
            full_path, node_id))
        return node_id

    @retry(5, 1.0, (httpx.ConnectError, NodeDoesNotExist))
    async def update_node(self, node_name, data):
        log.debug("Updating node {} with data {}".format(node_name, data))
        try:
            await self._session.put(
                self.url_for(self.NODE_URL, node_name),
                json=data)
        except httpx.HTTPStatusError as error:
            if error.response.status_code == 400:
                log.error("Bad PUT request on node {} with value {}".format(
                    node_name, data))

    @retry(5, 1.0, (httpx.ConnectError, NodeDoesNotExist))
    async def update_nodes(self, data):
        log.debug("Executing a bulk node update with data: {}".format(data))
        try:
            await self._session.put(
                self.url_for(self.NODE_BULK_URL),
                json=data)
        except httpx.HTTPStatusError as error:
            if error.response.status_code == 400:
                log.error("Bad PUT request on bulk nodes with value {}".format(
                    data))

    @retry(5, 1.0, (httpx.ConnectError, NodeDoesNotExist))
    async def get_node_history(self, node_name,
                               start=None, end=None,
                               limit=None,
                               groupby=None, agg_fn=None, agg_field=None,
                               time_interval=None, offset_interval=None):
        params = {}
        if start:
            params["from"] = start
        if end:
            params["to"] = end
        if limit:
            params["limit"] = limit
        if groupby:
            params["groupby"] = groupby
        if agg_fn:
            params["agg_fn"] = agg_fn
            params["agg_field"] = agg_field
        if time_interval:
            params["time_interval"] = time_interval
        if offset_interval:
            params["offset_interval"] = offset_interval

        response = await self._session.get(
            self.url_for(self.NODE_URL,
                         "{}/history".format(node_name),
                         params=params))
        return response.json()

    async def get_node_history_resampled(self, node_name, time_interval, start=None, end=None, limit=None,
                                         offset_interval=None):
        """
        Special node_history call to get data with a resampling interval.
        :param node_name: Node's name.
        :param time_interval: Resampling interval in format [Nu] (N=Number, u=unit; eg.: 3s, 5m).
        :param start: Start-Time (unix timestamp string).
        :param end: End-Time (unix timestamp string).
        :param limit: Max number of entries to return.
        :param offset_interval: Offset value for the group-by query.
        :return: List of dicts.
        """

        # Query history data by using FIRST function which returns the FIRST per interval.
        return await self.get_node_history(node_name, limit=limit, groupby='time', agg_fn='FIRST',
                                           agg_field='error_state', time_interval=time_interval,
                                           offset_interval=offset_interval, start=start, end=end)

    async def get_node_history_max(self, node_name, column_name, time_interval, start=None, end=None, limit=None,
                                   offset_interval=None):
        """
        Special node_history call to get the maximum of certain column.
        :param node_name: Node's name.
        :param column_name: Name of column to get the MAX for.
        :param time_interval: Resampling interval in format [Nu] (N=Number, u=unit; eg.: 3s, 5m).
        :param start: Start-Time (unix timestamp string).
        :param end: End-Time (unix timestamp string).
        :param limit: Max number of entries to return.
        :param offset_interval: Offset value for the group-by query.
        :return: List of dicts.
        """

        # Query history data by using FIRST function which returns the MAX per interval.
        return await self.get_node_history(node_name, limit=limit, groupby='time', agg_fn='MAX',
                                           agg_field=column_name, time_interval=time_interval,
                                           offset_interval=offset_interval, start=start, end=end)

    @retry(5, 0.2, (httpx.ConnectError, NodeDoesNotExist))
    async def get_node(self, node_name):
        response = await self._session.get(
            self.url_for(self.NODE_URL, node_name))
        return response.json()

    @retry(5, 0.2, (httpx.ConnectError, NodeDoesNotExist))
    async def delete_node(self, node_name):
        response = await self._session.delete(
            self.url_for(self.NODE_URL, node_name))
        return response.json()

    @retry(5, 1.0, (httpx.ConnectError, NodeDoesNotExist))
    async def get_tree(self, node_name):
        response = await self._session.get(
            self.url_for(self.TREE_URL, node_name))
        return response.json()

    async def build_node_tree(self, parent):
        def descend(branch, node, parent):
            node.node_id = branch["id"]
            node.node_type = branch["type"]
            node.node_name = branch["data"]["node_name"]
            node.config = branch["data"]
            node.parent = parent
            for child in branch["children"]:
                new_node = node.add_node(child["data"]["node_name"], child["type"], node)
                descend(child, new_node, node)

        tree = await self.get_tree(parent)
        root = Node(parent, Node.DEVICES, parent=None)
        if tree.get("code", "") != "200":
            descend(tree, root, None)
        return root

    @retry(5, 0.2, (httpx.ConnectError,))
    async def get_pending_states(self, task_ids):
        # TODO: Format for this call is not yet formalised
        log.debug("Fectching pending requests")
        response = self._session.get(
            self.url_for(self.NODE_URL, params={
                "parent_name": self._parent_device_name,
                "pending_request": self.TASK_PENDING}))
        if not response.text:
            return []
        return response.json()

    @retry(5, 0.2, (httpx.ConnectError,))
    async def set_pending_state(self, task_id, state, last_error_message=""):
        log.debug("Setting pending state on {} to {}".format(
            task_id, state))
        await self._session.put(
            self.url_for(self.NODE_URL, task_id),
            json={
                "pending_request": state,
                "last_error_message": last_error_message
            })

    async def create_query_group(self, pending_state=None):
        if not pending_state:
            pending_state = self.TASK_PENDING
        qg = QueryGroup(self, pending_state)
        await qg.create()
        return qg


class QueryGroup(object):
    def __init__(self, gim, pending_state=GIM.TASK_PENDING):
        self._gim = gim
        self._node_ids = set()
        self._set_id = None
        self._pending_state = pending_state
        self._synced = False

    @guarantee
    async def create(self):
        log.debug("Creating query group")
        response = await self._gim._session.post(
            self._gim.url_for(GIM.NODE_QUERY_URL),
            json={"node_id": [], "pending_request": self._pending_state}
        )
        self._set_id = response.json()["nodes_query_id"]
        log.info(f"Query group ID: {self._set_id}")

    @guarantee
    async def _update(self):
        if not self._node_ids:
            log.error("Cannot PUT and empty query set")
            return
        log.debug(f"{self._set_id}: Updating query group")
        await self._gim._session.put(
            self._gim.url_for(GIM.NODE_QUERY_URL,
                              self._set_id),
            json={
                "node_id": list(self._node_ids),
                "pending_request": self._pending_state
            }
        )
        self._synced = True

    async def add_nodes(self, node_ids):
        log.debug(f"{self._set_id}: Adding nodes to group {node_ids}")
        if set(node_ids).issubset(self._node_ids):
            log.debug(f"{self._set_id}: All nodes in group")
            return
        for node_id in node_ids:
            log.debug(f"{self._set_id}: Adding node {node_id}")
            self._node_ids.add(node_id)
        await self._update()

    async def remove_nodes(self, node_ids):
        log.debug(f"{self._set_id}: Removing nodes to group {node_ids}")
        if not any(node in self._node_ids for node in node_ids):
            return
        for node_id in node_ids:
            log.debug(f"{self._set_id}: Removing node {node_id}")
            self._node_ids.remove(node_id)
        await self._update()

    @retry(5, 0.2, (httpx.ConnectError,))
    async def query(self):
        log.debug(f"{self._set_id}: Querying group")
        if not self._node_ids or not self._synced:
            log.debug(f"{self._set_id}: Group is empty, returning []")
            return []
        response = await self._gim._session.get(
            self._gim.url_for(
                GIM.NODE_QUERY_URL,
                self._set_id
            )
        )
        return response.json()

    @retry(5, 0.2, (httpx.ConnectError,))
    async def delete(self):
        log.debug(f"{self._set_id}: Deleting group")
        await self._gim._session.delete(
            self._gim.url_for(
                GIM.NODE_QUERY_URL,
                self._set_id
            )
        )
        self._set_id = None
