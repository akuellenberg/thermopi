# igui_exporter

# Build docker image
docker build -t  igui_exporter .

# Run the docker image
 docker run --host katcp_host  --port katcp_port --igui-url http://134.104.16.96 --igui-user user --igui-pass pass --igui-parent <parent_name>:: --log-level debug

# Example
docker run igui_exporter --host 134.104.24.160  --port 53125 --igui-url http://134.104.16.96 --igui-user <username> --igui-pass <pass> --igui-parent EPT_TIC:: --log-level DEBUG

