import signal
import logging
import tornado
import json
import asyncio
import httpx
import time
import random
import sys
from katcp import Sensor
from tornado.gen import coroutine, sleep, Return
from tornado.platform.asyncio import AsyncIOMainLoop, to_asyncio_future
from argparse import ArgumentParser
from gim import Node, GIM, NodeDoesNotExist
from sidecar import Handler, KatcpSidecar

log = logging.getLogger("gim-exporter")

KATCP_DELIMITER = "."
VALID_STATES = [1, 2, 3, 4, 5, 6]
MAX_QUEUE_DEPTH = 100
MAX_GIM_VALUE_SIZE = 1000


# TODO
# polling interval as cli arg -- Done!
# don't use repr(error) for last_error_message -- Done
# allow for arbitrary offset on startup

DEFAULT_SAMPLING_STRATEGY = ["period", 30]

STATS = {"heartbeat": {"successes": 0, "failures": 0},
         "sensors": {}}


class RequestError(Exception):
    pass


def icom_request_string_parser(request_help):
    float_delim = "..."
    options_delim = "|"
    request = {}
    desc, signature = request_help.split("(?")
    desc = desc.strip()
    request["description"] = desc
    signature = signature.strip(")")
    request_name, params = signature.split("[")
    request["name"] = request_name.strip("?").strip()
    params = params.strip("[]")
    if float_delim in params:
        request["mysql_task_type"] = "GET_SET"
        lower, upper = map(float, params.split(float_delim))
        request["upper_limit"] = upper
        request["lower_limit"] = lower
        request["value_type"] = "float"
    elif options_delim in params:
        request["mysql_task_type"] = "DROPDOWN"
        request["options"] = {i: i for i in params.split(options_delim)}
        request["value_type"] = "str"
    else:
        request["mysql_task_type"] = "GET_SET"
        request["value_type"] = "str"
    return request


def sanitize_numeric_value(value):
    inf_mapping = {
        float('inf'): sys.float_info.max,
        float('-inf'): -sys.float_info.max
        }
    try:
        update_value = inf_mapping[value]
        log.debug("Mapping unsupported sensor value for {} -> {}".format(
            value, update_value))
    except KeyError:
        update_value = value
    return update_value


class UpdateQueueManager:
    def __init__(self):
        self._queues = {}

    def put(self, node_id, message):
        if node_id not in self._queues:
            queue = asyncio.Queue(maxsize=MAX_QUEUE_DEPTH)
            self._queues[node_id] = queue
        else:
            queue = self._queues[node_id]
        try:
            queue.put_nowait(message)
        except asyncio.QueueFull as error:
            log.warning("QueueFull for node_id {}".format(node_id))

    def getall(self):
        output = {}
        for node_id, queue in self._queues.items():
            if node_id not in output:
                output[node_id] = []
            while not queue.empty():
                output[node_id].append(queue.get_nowait())
            if not output[node_id]:
                del output[node_id]
        return output


class GIMExporter(Handler):
    def __init__(self, url, user,
                 password, parent_device_name,
                 polling_interval=3.0,
                 update_interval=3.0,
                 valid_istates=None,
                 override_gim=False,
                 policies=None,
                 default_sampling_strategy=None,
                 set_get_sleep=1.0):
        """
        @brief    Construct an InfluxDBExporter
        """
        super(GIMExporter, self).__init__()
        self._gim = GIM(url, user, password)
        self._task_map = {}
        self._parent_device_name = parent_device_name
        self._polling_interval = polling_interval
        self._update_interval = update_interval
        self._valid_istates = VALID_STATES if valid_istates is None else valid_istates
        self._ioloop = AsyncIOMainLoop()
        self._stop = False
        self._client = None
        self._root_node = Node(parent_device_name, Node.DEVICES, None)
        self._query_group = None
        self._query_group_stale = True
        self._override_gim = override_gim
        self._policies = policies if policies is not None else ["ignore_unknown"]
        if default_sampling_strategy is None:
            self._default_sampling_strategy = {
                "type": DEFAULT_SAMPLING_STRATEGY[0],
                "args": DEFAULT_SAMPLING_STRATEGY[1:]
            }
        else:
            self._default_sampling_strategy = {
                "type": default_sampling_strategy[0],
                "args": default_sampling_strategy[1:]
            }
        self._set_get_sleep = set_get_sleep
        self._queue_manager = UpdateQueueManager()
        log.info(f"Default sampling strategy: {self._default_sampling_strategy}")
        log.info(f"Policies: {self._policies}")

    async def init(self):
        while True:
            try:
                await self._gim.authorize()
            except httpx.ConnectError as error:
                log.error("Unable to connect to iGUI waiting 10 seconds")
                await asyncio.sleep(10)
            except Exception as error:
                log.exception(f"Error on authentication: {error}")
                await self._gim.shutdown()
                raise error
            else:
                break
        self._query_group = await self._gim.create_query_group(GIM.TASK_PENDING)

    def update_config(self, config):
        self._root_node.update_from_config(config)

    async def startup_hook(self, client):
        # Here we do the device info lookup
        self._client = client
        log.debug("Checking for ?gim-config call")
        if "gim_config" in self._client.req.keys():
            log.debug("?gim-config call exists")
            response = await to_asyncio_future(self._client.req.gim_config(timeout=10))
            if response.reply.reply_ok():
                config = json.loads(response.reply.arguments[1].decode())
            else:
                log.exception("Error on ?gim-config call to server: {}".format(
                    response.reply.arguments[1]))
            self.update_config(config)
            while True:
                try:
                    await self._root_node.sync_with_gim(
                        self._gim, update=self._override_gim)
                except httpx.HTTPStatusError as error:
                    if error.response.status_code in (500, 503):
                        log.warning(
                            f"Server busy, waiting and retrying ({error.response.status_code} response code)")
                        await asyncio.sleep(5)
                else:
                    break
        else:
            log.warning("?gim-config call does not exist")
        # Here need to decide how to merge configurations
        self.start()

    async def _autogen_icom_task(self, sensor, params):
        # First try to make an ICOM specific node
        # if fails, then fall back to simple GET
        # TODO: implement icom logic
        if "request" not in params or params["request"] is None:
            node = await self._autogen_task(sensor, params)
            return node
        config = {
            "options": "get_set"
        }
        try:
            config["metadata"] = icom_request_string_parser(
                params["request"]["description"])
        except Exception as error:
            log.exception(repr(error))
            node = await self._autogen_task(sensor, params)
        else:
            node = await self._autogen_task(sensor, params, config=config)
        return node

    async def _autogen_task(self, sensor, params, config=None):
        """
        Autogeneration of a task from a sensor name

        Currently this only supports GET type
        tasks and does not do a corresponding request
        look up
        """
        if config is None:
            opt_set = "get_set" if params["request"] else "get"
            config = {
                "options": opt_set,
                "metadata": {"mysql_task_type": opt_set.upper()}
            }
        if "units" in params and params["units"] != "discrete":
            config["metadata"]["unit"] = params["units"]
        if "description" in params:
            config["metadata"]["description"] = params["description"]
        if "upper_limit" in params:
            config["metadata"]["upper_limit"] = params["upper_limit"]
        if "lower_limit" in params:
            config["metadata"]["lower_limit"] = params["lower_limit"]
        if "value_type" in params:
            config["metadata"]["value_type"] = params["value_type"]

        log.info(f"Autogenerating task {sensor.name} with config {config}")
        node = self._root_node.add_node(
            sensor.name, Node.TASKS,
            config, KATCP_DELIMITER)
        log.debug(f"Added node {node}")
        # Here we can force an update to the sensor map
        self._root_node.sensor_lookup(sensor.name, refresh=True)
        return node

    async def finalise_node_registration(self):
        self._root_node.inject_sidecars()
        await self._root_node.sync_with_gim(
            self._gim, update=self._override_gim)
        self._query_group_stale = True

    async def register_sensor(self, sensor, extra_params=None):
        log.info("Registering sensor {}".format(sensor.name))
        log.debug("Sensor parameters {}".format(extra_params))
        try:
            node = self._root_node.sensor_lookup(sensor.name, refresh=True)
            log.debug("Sensor already known")
        except KeyError:
            if "ignore_unknown" in self._policies:
                log.info("ignore_unknown policy invoked, ignoring sensor")
                return {"type": "none", "args": []}
            elif "autogen_icom_task" in self._policies:
                log.info("autogen_icom_task policy invoked, generating sensor")
                node = await self._autogen_icom_task(sensor, extra_params)
            elif "autogen_task" in self._policies:
                log.info("autogen_task policy invoked, generating sensor")
                node = await self._autogen_task(sensor, extra_params)
            else:
                return {"type": "none", "args": []}

        # Add node to node set
        self._query_group_stale = True
        try:
            return node.get_config_parameter("sampling_strategy")
        except KeyError:
            # return {'type': 'event-rate', 'args': [30, 300]}
            return self._default_sampling_strategy

    async def heartbeat(self):
        def task_selector(task):
            if task.parent is None:
                return False
            else:
                return all([
                    task.node_name == "connected",
                    task.parent.node_name == "sidecar"
                ])

        STATS["heartbeat-count"] = 0

        updates = {}
        while True:
            state = self._client.is_connected()
            futures = []
            timestamp = "{:.6f}".format(time.time())
            for task in self._root_node.list_tasks(filter_func=task_selector):
                log.debug("Heartbeat on task {}".format(task))

                updates[task.node_id] = [
                    {
                        "current_value": int(state),
                        "timestamp": timestamp,
                        "error_state": Sensor.NOMINAL,
                        "value_type": "int"
                    }
                ]

            try:
                await self._gim.update_nodes(updates)
            except httpx.ConnectError as error:
                log.error("No connection to iGUI, heartbeat failed")
                STATS["heartbeat"]["failures"] += 1
            except Exception as error:
                log.error("Error on heartbeat update: {}".format(str(error)))
                STATS["heartbeat"]["failures"] += 1
            finally:
                STATS["heartbeat"]["successes"] += 1
                await asyncio.sleep(60)

    async def stats_update(self):
        while True:
            log.info("Heartbeats -- Success: {} -- Failure: {}".format(
                STATS["heartbeat"]["successes"],
                STATS["heartbeat"]["failures"]))
            log.info(
                "Sensor updates: SENSOR -- Received / Pushed / Failed push / Ignored (task limit) / Ignored (status)")
            for key, stats in STATS["sensors"].items():
                log.info("{} -- {} / {} / {} / {} / {}".format(
                    key,
                    stats["received_updates"],
                    stats["pushed_updates"],
                    stats["failed_updates"],
                    stats["ignored_tasklimit"],
                    stats["ignored_status"]))
            await asyncio.sleep(60)

    @coroutine
    def sensor_update(self, sensor, reading):
        self._sensor_update(sensor, reading)

    def _sensor_update(self, sensor, reading):
        """
        Take action on an update to a KATCP sensor

        :param      sensor:   The sensor
        :param      reading:  The reading

        :note       This is used as a callback method on a KATCPResourceClient
        :note       This must stay as a tornado coroutine as it passed into the KATCP library
        """

        if sensor.name not in STATS["sensors"]:
            STATS["sensors"][sensor.name] = {
                "received_updates": 1,
                "pushed_updates": 0,
                "failed_updates": 0,
                "ignored_tasklimit": 0,
                "ignored_status": 0}
        else:
            STATS["sensors"][sensor.name]["received_updates"] += 1

        log.debug(
            "Received update for sensor {} with reading {}".format(
                sensor.name, reading))
        if reading.istatus not in self._valid_istates:
            log.debug(
                "Handler ignoring reading with invalid istatus ({})".format(
                    reading.istatus))
            STATS["sensors"][sensor.name]["ignored_status"] += 1
            return
        try:
            node = self._root_node.sensor_lookup(sensor.name, refresh=False)
        except KeyError:
            log.warning(
                "Recieved update for untracked sensor: {}".format(
                    sensor.name))
            return
        else:
            log.debug("Found associated node: {}".format(node))
            node.last_sensor_reading = reading
            try:
                if sensor.type in [Sensor.INTEGER, Sensor.FLOAT]:
                    update_value = sanitize_numeric_value(value)
                else:
                    update_value = reading.value

                try:
                    value_type = node.get_config_parameter("value_type")
                except KeyError as error:
                    log.warning("Could not deduce value type for sensor")
                    value_type = "float"

                data = {
                    "timestamp": "{:.6f}".format(reading.timestamp),
                    "error_state": reading.istatus,
                    "data": None,
                    "current_value": None,
                    "value_type": value_type
                }
                exceeds_size_limit = False
                # Workaround to make IMAGE type sensors working
                if '-image' in node.task_name():
                    data["data"] = update_value
                    self._queue_manager.put(node.node_id, data)
                else:
                    data["current_value"] = update_value

                    try:
                        if len(update_value) > MAX_GIM_VALUE_SIZE:
                            exceeds_size_limit = True
                    except TypeError:
                        pass
                if not exceeds_size_limit:
                    self._queue_manager.put(node.node_id, data)
                else:
                    log.warning("Update would exceed max GIM value size, skipping update.")
            except Exception as error:
                log.exception("Exception while pushing update to queue: {}".format(
                    repr(error)))
                STATS["sensors"][sensor.name]["failed_updates"] += 1
            else:
                STATS["sensors"][sensor.name]["pushed_updates"] += 1

    async def handle_request(self, task):
        log.info(
            "Received pending request on task {}".format(
                task["node_name"]))
        try:
            node = self._root_node.task_lookup_by_id(task["node_id"])
        except NodeDoesNotExist:
            log.exception("Cannot execute pending request on node {}".format(
                task["node_id"]))
            await self._gim.set_pending_state(
                task["node_id"], GIM.TASK_FAILED,
                last_error_message="No such task with ID {} and name {}".format(
                    task["node_id"], task["node_name"]))
            return
        request_name = node.request_name()
        if not request_name:
            log.exception(
                "Cannot execute pending request on node {}: no KATCP request".format(
                    task["node_id"]))
            await self._gim.set_pending_state(
                task["node_id"], GIM.TASK_FAILED,
                last_error_message="Setting of this task is not supported")
            return

        await self._gim.set_pending_state(task["node_id"], GIM.TASK_RUNNING)
        log.info("Setting the value of {} to {}".format(
            task["node_name"], task["desired_value"]))
        try:
            log.debug("Calling request {}".format(request_name))
            response = await to_asyncio_future(
                self._client.req[request_name.replace("-", "_")](
                    task["desired_value"],
                    timeout=5.0))
            if not response.reply.reply_ok():
                try:
                    reason = response.reply.arguments[1].decode()
                except Exception as error:
                    reason = "unknown reason"
                raise RequestError(
                    f"Error on {request_name} request: {reason}")

            try:
                await asyncio.sleep(self._set_get_sleep)
                response = await to_asyncio_future(self._client.req.sensor_value(node.sensor_name()))
                if not response.reply.reply_ok():
                    log.warning(
                        f"Could not force update for sensor {node.sensor_name()}")
            except Exception as error:
                log.warning(
                    f"Could not force update for sensor {node.sensor_name()}: {repr(error)}")

        except Exception as error:
            log.error(
                "Unable to set value of {} to {} with error: {}".format(
                    task["node_name"], task["desired_value"], repr(error)))
            await self._gim.set_pending_state(
                task["node_id"], GIM.TASK_FAILED,
                last_error_message=repr(error))
        else:
            await self._gim.set_pending_state(
                task["node_id"], GIM.TASK_COMPLETE)

    def start(self):
        asyncio.ensure_future(self.heartbeat())
        asyncio.ensure_future(self.stats_update())
        asyncio.ensure_future(self.handle_pending_requests())
        asyncio.ensure_future(self.push_updates())

    def stop(self):
        self._ioloop.clear_current()

    async def push_updates(self):
        log.info(f"Starting bulk update loop with {self._update_interval} second interval")
        while True:
            try:
                log.debug("Checking for updates to be pushed to GIM")
                updates = self._queue_manager.getall()
                if updates:
                    log.debug("Pushing {} node updates to GIM".format(len(updates)))
                    await self._gim.update_nodes(updates)
            except Exception as error:
                log.error("Error while pushing updates: {}".format(
                    repr(error)))
            finally:
                await asyncio.sleep(self._update_interval)

    async def handle_pending_requests(self):
        log.info(f"Starting pending state loop with {self._polling_interval} second interval")
        while True:
            try:
                tasks = await self.get_pending_requests()
                log.debug("Found {} tasks with pending updates".format(
                    len(tasks)))
                for task in tasks:
                    await self.handle_request(task)
            except Exception as error:
                log.error("Error while handling pending requests: {}".format(
                    repr(error)))
            finally:
                await asyncio.sleep(self._polling_interval)

    async def update_query_group(self):
        self._query_group_stale = False
        tasks = self._root_node.list_tasks(
            filter_func=lambda node:
                node.config.get("options", "") in ["set", "get_set"])
        task_ids = []
        for task in tasks:
            await task._sync_node(self._gim)
            task_ids.append(task.node_id)
        log.info("Adding {} task IDs to query group".format(len(task_ids)))
        log.debug("Adding task_ids to query group: {}".format(task_ids))
        await self._query_group.add_nodes(task_ids)

    async def get_pending_requests(self):
        if self._query_group_stale:
            await self.update_query_group()
        return await self._query_group.query()


@coroutine
def on_shutdown(ioloop, client, handler):
    handler.stop()
    log.info("Shutting down client")
    client.stop()
    ioloop.stop()


async def main(args):
    FORMAT = "[ %(levelname)s - %(asctime)s - %(filename)s:%(lineno)s] %(message)s"
    logger = logging.getLogger('gim-exporter')
    logging.basicConfig(format=FORMAT)
    logger.setLevel(args.log_level.upper())
    logging.getLogger('gim-api-python').setLevel(args.log_level.upper())
    logging.getLogger('katcp-sidecar').setLevel(args.log_level.upper())
    logging.getLogger('katcp').setLevel('INFO')
    ioloop = AsyncIOMainLoop()
    log.info("Starting KATCPToGIMConverter instance")
    handler = GIMExporter(
        args.gim_url,
        args.gim_user,
        args.gim_pass,
        args.gim_parent,
        polling_interval=args.polling_interval,
        update_interval=args.update_interval,
        override_gim=args.override_gim,
        policies=args.policies,
        default_sampling_strategy=args.default_sampling_strategy,
        set_get_sleep=args.set_get_sleep
    )

    # Sleep for the stagger time
    stagger = random.uniform(0, args.stagger)
    log.info(f"Staggering sidecar start by {stagger} seconds")
    await asyncio.sleep(random.uniform(0, args.stagger))

    try:
        await handler.init()
    except Exception as error:
        loop = asyncio.get_event_loop()
        loop.stop()

    if args.icinga_host:
        log.info(f"Setting icinga_host flag on {args.icinga_host}")
        try:
            node = await handler._gim.get_node(args.icinga_host)
            node["metadata"]["icinga_host"] = 1
            await handler._gim.update_node(
                args.icinga_host, {"metadata": node["metadata"]})
        except Exception as error:
            log.error("Unable to set the icinga-host flag on {} with error: {}".format(
                args.icinga_host, repr(error)))

    client = KatcpSidecar(args.host, args.port, handler)

    signal.signal(
        signal.SIGINT,
        lambda sig, frame: ioloop.add_callback_from_signal(
            on_shutdown, ioloop, client, handler))

    await client.start()
    log.info("Ctrl-C to terminate client")


def parse_args():
    usage = "usage: %prog [options]"
    parser = ArgumentParser(description=usage)
    parser.add_argument('--host', action='store', dest='host', type=str,
                        help='The hostname for the KATCP server to connect to',
                        required=True)
    parser.add_argument(
        '--port', action='store', dest='port', type=int,
        help='The port number for the KATCP server to connect to',
        required=True)
    parser.add_argument('--gim-url', action='store', dest='gim_url',
                        type=str, help='The GIM URL address to connect to',
                        required=True)
    parser.add_argument('--gim-user', action='store', dest='gim_user',
                        type=str, help='The GIM username', required=True)
    parser.add_argument('--gim-pass', action='store', dest='gim_pass',
                        type=str, help='The GIM password', required=True)
    parser.add_argument('--gim-parent', action='store', dest='gim_parent',
                        type=str, help='The GIM parent ID', required=True)
    parser.add_argument(
        '--override-gim',
        action='store_true',
        dest='override_gim',
        help=(
            'When katcp server and gim metadata conflicts, '
            'choose katcp server version'))
    parser.add_argument(
        '--polling-interval',
        action='store',
        dest='polling_interval',
        type=float,
        help='The polling interval for pending requests',
        default=1.0)
    parser.add_argument(
        '--update-interval',
        action='store',
        dest='update_interval',
        type=float,
        help='The time interval between pushing updates to the GIM',
        default=1.0)
    parser.add_argument(
        '--stagger',
        action='store',
        dest='stagger',
        type=float,
        help='Stagger the start of this sidecar by a random number of seconds with maximum value N',
        default=0.0)
    parser.add_argument(
        '--default-sampling-strategy',
        action='store',
        nargs='+',
        dest='default_sampling_strategy',
        type=str,
        help='Set the default sampling strategy to be used for the server [takes full strategy description, e.g. event-rate 30 500]',
        default=DEFAULT_SAMPLING_STRATEGY)
    parser.add_argument(
        '--policy',
        action='append',
        type=str,
        dest='policies',
        help='specify a policy to be used when encountering unknown sensors or servers without a ?gim-config call.',
        default=None)
    parser.add_argument(
        '--icinga-host',
        action='store',
        type=str,
        dest='icinga_host',
        help='specify a gim node to be tagged as the icinga-host for this sidecar (optional)',
        default=None)
    parser.add_argument(
        '--set-get-sleep',
        type=float,
        dest='set_get_sleep',
        help='Number of seconds to sleep between setting a request and retrieiving its value',
        default=1.0)
    parser.add_argument(
        '--log-level',
        action='store',
        dest='log_level',
        type=str,
        help='Logging level',
        default="INFO")
    args = parser.parse_args()
    FORMAT = "[ %(levelname)s - %(asctime)s - %(filename)s:%(lineno)s] %(message)s"
    logger = logging.getLogger('gim-exporter')
    logging.basicConfig(format=FORMAT)
    logger.setLevel(args.log_level.upper())
    logging.getLogger('gim-api-python').setLevel(args.log_level.upper())
    logging.getLogger('katcp-sidecar').setLevel(args.log_level.upper())
    logging.getLogger('katcp').setLevel('INFO')
    return args


if __name__ == "__main__":
    args = parse_args()
    loop = asyncio.get_event_loop()
    loop.create_task(main(args))
    loop.run_forever()
