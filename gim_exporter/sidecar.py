import logging
import signal
import tornado
import time
import json
import asyncio
import katcp
from abc import ABCMeta, abstractmethod
from tornado.gen import coroutine, Return
from tornado.platform.asyncio import AsyncIOMainLoop, to_asyncio_future
from argparse import ArgumentParser
from katcp import KATCPClientResource

log = logging.getLogger("katcp-sidecar")

FALLBACK_SAMPLING_STRATEGY = {
    "type": "period",
    "args": [600]
}


class Handler(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        pass

    @abstractmethod
    async def startup_hook(self, client):
        pass

    @abstractmethod
    def register_sensor(self, sensor, extra_params=None):
        pass

    @abstractmethod
    def sensor_update(self, sensor, reading):
        pass


class NoOpHandler(Handler):
    def __init__(self):
        super(NoOpHandler, self).__init__()

    async def startup_hook(self, client):
        log.debug("Startup hook called on handler with client: {}".format(
            client))
        pass

    def register_sensor(self, sensor, extra_params=None):
        log.debug("Register sensor called on handler")
        # return a sampling strategy
        return ("auto")

    def sensor_update(self, sensor, reading):
        log.debug("Sensor update called on handler for sensor: {} with reading: {}".format(
            sensor, reading))
        pass


class KatcpSidecar(object):
    def __init__(self, host, port, handler=None):
        """
        Constructs a new instance.

        :param      host:  The address of the server to sidecar
        :param      port:  The server port
        """
        log.debug("Constructing sidecar for {}:{}".format(host, port))
        self.rc = KATCPClientResource(dict(
            name="sidecar-client",
            address=(host, port),
            controlled=True))
        self._handler = handler if handler is not None else NoOpHandler()
        self._previous_sensors = set()
        self._inflight_tasks = []

    async def start(self):
        """
        @brief     Start the sidecar
        """
        async def _start():
            log.debug("Connecting to KATCP server")
            base_timeout = 10.0
            max_timeout = 300.0
            timeout = base_timeout
            while True:
                try:
                    await to_asyncio_future(self.rc.until_synced(timeout=5.0))
                except tornado.gen.TimeoutError:
                    log.error(f"Timeout on sync with KATCP server, retrying in {timeout} seconds")
                    await asyncio.sleep(timeout)
                    timeout *= 1.5
                    if timeout > max_timeout:
                        timeout = max_timeout
                else:
                    break
            log.debug("Client synced")
            log.debug("Requesting version info")
            response = await to_asyncio_future(self.rc.req.version_list())
            log.info("Server version info: {}".format(response.reply.arguments))
            await self._handler.startup_hook(self.rc)
            self.ioloop.add_callback(self.on_interface_changed)
        ioloop = AsyncIOMainLoop()
        self.rc.set_ioloop(ioloop)
        self.rc.start()
        self.ic = self.rc._inspecting_client
        self.ioloop = ioloop
        self.ic.katcp_client.hook_inform(
            "interface-changed",
            lambda message: self.ioloop.add_callback(
                self.on_interface_changed))
        self.ic.katcp_client.hook_inform(
            "gim-config-changed",
            lambda message: self.ioloop.add_callback(
                self._handler.startup_hook, self.rc))
        await _start()
        asyncio.ensure_future(self.task_cleanup())

    def stop(self):
        """
        @brief      Stop the sidecar
        """
        self.rc.stop()

    async def task_cleanup(self):
        while True:
            while self._inflight_tasks:
                task = self._inflight_tasks.pop(0)
                try:
                    await task
                except Exception as error:
                    log.exception("Caught exception from task: {}".format(
                        repr(error)))
            await asyncio.sleep(30)

    @coroutine
    def on_interface_changed(self):
        task = asyncio.ensure_future(self._on_interface_changed())
        self._inflight_tasks.append(task)

    async def parse_sensor_inform(self, params):
        out = {}
        out["name"] = params.pop(0).decode()
        out["description"] = params.pop(0).decode()
        out["units"] = params.pop(0).decode()
        out["value_type"] = params.pop(0).decode()
        extra_args = [i.decode() for i in params]
        if out["value_type"].lower() in ["float", "integer"]:
            if len(extra_args) != 2:
                log.warning("Sensor does not match upper/lower limit specification")
            try:
                upper = float(extra_args[0])
                lower = float(extra_args[1])
                out["upper_limit"] = max(upper, lower)
                out["lower_limit"] = min(upper, lower)
            except Exception as error:
                log.warning(f"Unable to parse upper/lower limits with error: {repr(error)}")
        elif out["value_type"].lower() == "discrete":
            out["options"] = extra_args
        else:
            log.debug("Value type of {} detected, taking no action".format(out["value_type"]))
        out["sensor_parameters"] = extra_args
        request_name_guess = out["name"].replace(".", "-")
        log.debug(f"Checking for request: {request_name_guess}")
        response = await to_asyncio_future(self.rc.req.help(request_name_guess))
        if response.reply.reply_ok():
            log.debug("Request found")
            args = response.informs[0].arguments
            out["request"] = {
                "name": args.pop(0).decode(),
                "description": args.pop(0).decode(),
                }
        else:
            out["request"] = None
        return out

    @coroutine
    def set_sampling_strategy_safe(self, name, strategy):
        passed, msg = yield self.rc.sensor[name].set_sampling_strategy(strategy)
        if not passed:
            log.warning(f"Unable to set strategy {strategy} on sensor {name}, using fallback strategy")
            passed, msg = yield self.rc.sensor[name].set_sampling_strategy(
                (FALLBACK_SAMPLING_STRATEGY["type"],
                 *FALLBACK_SAMPLING_STRATEGY["args"]))
            if not passed:
                log.exception(f"Unable to set any sampling strategy on sensor {name}: {msg}")

    async def _on_interface_changed(self):
        """
        @brief    Synchronise with the sidecar'd servers new sensors
        """
        log.debug("Waiting on synchronisation with server")
        await to_asyncio_future(self.rc.until_synced())
        log.debug("Client synced")
        current_sensors = set(self.rc.sensor.keys())
        log.debug("Current sensor set: {}".format(current_sensors))
        removed = self._previous_sensors.difference(current_sensors)
        log.debug("Sensors removed since last update: {}".format(removed))
        added = current_sensors.difference(self._previous_sensors)
        log.debug("Sensors added since last update: {}".format(added))
        await to_asyncio_future(self.rc.until_synced())
        sensor_names = [self.rc.sensor[name].name for name in list(added)]

        for ii, name in enumerate(list(added)):
            log.debug("Handling sensor {} of {}".format(ii+1, len(added)))
            log.debug("Retrieving extra parameters for sensor '{}'".format(name))
            try:
                response = await self.make_request("sensor-list", self.rc.sensor[name].name)
                extra_params = await self.parse_sensor_inform(response.informs[0].arguments)
            except Exception as error:
                log.exception("Unable to call sensor-list on sensor '{}' with error: {}".format(
                    self.rc.sensor[name].name, repr(error)))
                extra_params = None
            log.debug("Registering sensor with handler")
            strategy = await self._handler.register_sensor(self.rc.sensor[name], extra_params)
            log.debug("Handler requests a sampling strategy of {}".format(strategy))
            log.debug(
                "Setting sampling strategy and callbacks on sensor '{}'".format(name))
            await to_asyncio_future(self.set_sampling_strategy_safe(name, (strategy['type'], *strategy['args'])))

        await self._handler.finalise_node_registration()
        for name in list(added):
            await to_asyncio_future(self.rc.set_sensor_listener(name, self._handler.sensor_update))
        self._previous_sensors = current_sensors

    async def make_request(self, request_name, *args, **kwargs):
        """
        @brief Makes and execute a request to the server

        @param      request_name  The request name as a string (used for lookup)
        @param      args          The arguments (positional arguments for the request)
        @param      kwargs        The keywords arguments (keyword arguments for the request such as timeouts)
        """
        log.debug("Request for {} with args {} and kwargs {}".format(
            request_name, args, kwargs))
        request = self.rc.req[request_name.replace("-", "_")]
        start = time.time()
        response = await to_asyncio_future(request(*args, **kwargs))
        log.debug("Request to server took {} seconds".format(
            time.time() - start))
        log.debug("Reply: {}".format(response.reply.arguments))
        for inform in response.informs:
            log.debug("Inform: {}".format(inform.arguments))
        if not response.reply.reply_ok():
            raise Exception(response.reply.arguments[1])
        return response


