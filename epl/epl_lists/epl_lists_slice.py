def sublist_from_list_of_lists(source_list, start, stop, step=1):
    """
    Returns a sublist of lists from a list of lists.
    :param source_list: List of lists (the source list).
    :param start: Start index of slice (None for beginning).
    :param stop: Stop index for slice (None for end).
    :param step: Step size for slice.
    :return: Sublist of lists.
    """
    return [list_entry[start:stop:step] for list_entry in source_list]


def single_list_from_list_of_lists(source_list, index):
    """
    Returns a single list from a list of lists.
    :param source_list: List of lists (the source list).
    :param index: The index to be sliced.
    :return: Single list of items.
    """
    return [list_entry[index] for list_entry in source_list]


def sublist_of_lists_from_list_of_lists(source_list, index, length):
    """
    Returns a single list from a list of lists.
    :param source_list: List of lists (the source list).
    :param index: The index to be sliced.
    :param length: Number of columns to be sliced.
    :return: Single list of items.
    """
    return [list_entry[index:length] for list_entry in source_list]

class EplListsSlice:
    """
    Class providing abstract list slice functionality.
    """
    def __init__(self):
        pass