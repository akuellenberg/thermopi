# -*- coding: utf-8 -*-
"""
Copyright 2017 fmnisme@gmail.com, Copyright 2020 christian@jonak.org

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Icinga 2 API client base
"""

from __future__ import print_function
import logging
import sys
import httpx
import backoff

# pylint: disable=import-error,no-name-in-module
if sys.version_info >= (3, 0):
    from urllib.parse import urljoin
else:
    from urlparse import urljoin
# pylint: enable=import-error,no-name-in-module

from epl.epl_icinga2api.epl_icinga2api_exceptions import *

LOG = logging.getLogger(__name__)

DEFAULT_TIMEOUT = 10  # seconds


async def log_request(request):
    LOG.debug(f"Request event hook: {request.method} {request.url} - Waiting for response")
    pass


async def log_response(response):
    request = response.request
    LOG.debug(f"Response event hook: {request.method} {request.url} - Status {response.status_code}")


async def raise_on_4xx_5xx(response):
    response.raise_for_status()


def backoff_handler(details):
    """
    Backoff handler for backoff module.
    """
    LOG.warning("{target}: Backing off {wait:0.1f} seconds after {tries} tries "
                "calling function {target} with args {args} and kwargs "
                "{kwargs}".format(**details))


def give_up_handler(details):
    """
    Give-up handler for backoff module.
    """
    LOG.error("{target}: Giving up after {tries} tries "
              "calling function {target} with args {args} and kwargs "
              "{kwargs}".format(**details))


def success_handler(details):
    """
    Success handler for backoff module.
    """
    LOG.debug("{target}: Success after {tries} tries "
              "calling function {target} with args {args} and kwargs "
              "{kwargs}".format(**details))


class Base(object):
    """
    Icinga 2 API Base class
    """

    base_url_path = None  # 继承

    def __init__(self, manager):
        """
        initialize object
        """

        # Need LOG global to have it with backoff
        global LOG

        self.manager = manager
        LOG = logging.getLogger(self.manager.logger.logger.name)
        self.manager = manager
        self.logger = self.manager.logger
        self.stream_cache = ""
        self.session = self.manager.session
        self.httpx_max_connections = self.manager.httpx_max_connections

    def _create_session(self, method='POST'):
        """
        create a session object
        """
        ses_cert = None
        ses_auth = None
        if self.manager.certificate and self.manager.key:
            # certificate and key are in different files
            ses_cert = (self.manager.certificate, self.manager.key)
        elif self.manager.certificate:
            # certificate and key are in the same file
            ses_cert = self.manager.certificate
        elif self.manager.username and self.manager.password:
            # use username and password
            ses_auth = (self.manager.username, self.manager.password)

        ses_verify = False
        if self.manager.ca_certificate:
            ses_verify = self.manager.ca_certificate

        ses_headers = {
            'User-Agent': 'Python-epl_icinga2api/{0}'.format(self.manager.version),
            'X-HTTP-Method-Override': method.upper(),
            'Accept': 'application/json'
        }

        limits = httpx.Limits(max_connections=self.httpx_max_connections)
        transport = httpx.AsyncHTTPTransport(verify=False, retries=5)
        event_hooks = {
            'request': [log_request],
            'response': [log_response, raise_on_4xx_5xx]
        }

        session = httpx.AsyncClient(
            cert=ses_cert,
            auth=ses_auth,
            headers=ses_headers,
            limits=limits,
            transport=transport,
            timeout=DEFAULT_TIMEOUT,
            event_hooks=event_hooks,
            verify=ses_verify
        )

        return session

    @backoff.on_exception(backoff.expo,
                          httpx.RequestError,
                          max_tries=10,
                          on_backoff=backoff_handler,
                          on_giveup=give_up_handler,
                          on_success=success_handler)
    async def _request(self, method, url_path, payload=None, stream=False):
        """
        make the request and return the body

        :param method: the HTTP method
        :type method: string
        :param url_path: the requested url path
        :type url_path: string
        :param payload: the payload to send
        :type payload: dictionary
        :returns: the response as json
        :rtype: dictionary
        """
        request_url = urljoin(self.manager.url, url_path)

        # create session
        self.session = self._create_session(method)

        # create arguments for the request
        request_args = {
            'url': request_url
        }
        if payload:
            request_args['json'] = payload

        response = None

        # do the request
        try:
            async with self.session as client:
                response = await client.post(**request_args)
        except httpx.HTTPError as e:
            self.logger.write_error("_request: ex:{}, url:{}, payload:{}".format(e, url_path, payload))
        except Exception as u:
            self.logger.write_error("Error sending post request: {}, ex: {}".format(request_args, u))

        if not stream:
            await self.session.aclose()
        # # for debugging
        # from pprint import pprint
        # pprint(request_url)
        # pprint(payload)
        # pprint(response)
        if not response:
            return None
        if not 200 <= response.status_code <= 299:
            raise Icinga2ApiRequestException(
                'Request "{}" failed with status {}: {}'.format(
                    response.url,
                    response.status_code,
                    response.text,
                ), response.json())
        if stream:
            return response
        else:
            return response.json()

    @staticmethod
    def _get_message_from_stream(stream):
        """
        make the request and return the body

        :param stream: the stream
        :type method: request
        :returns: the message
        :rtype: dictionary
        """

        # TODO: test iter_lines()
        message = b''
        for char in stream.iter_content():
            if char == b'\n':
                yield message.decode('unicode_escape')
                message = b''
            else:
                message += char
