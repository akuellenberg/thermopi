import re

"""Mapping of wildcards to regex expressions"""
MapRegEx = {
    '*': '.+',
}

"""List with characters to be escaped"""
MapEscape = {
    '.': '\.',
}


def substring_character(source_string, left_char, right_char, left_start=0):
    """
    Searches a substring which is surrounded by left_character and right_character.
    :param source_string: The source string.
    :param left_char: Character surrounding on left side.
    :param right_char: Character surrounding on right side.
    :param left_start: Index to start search at.
    :return: Success->resulting substring; Others->None.
    """
    try:
        left_index = source_string.find(left_char, left_start)
        right_index = source_string.find(right_char, left_index + 1)
    except:
        return None
    finally:
        return source_string[left_index + 1:right_index]


def wildcard_search(source_string, search_pattern):
    """
    Search with wildcards.
    :param source_string: The source string.
    :param search_pattern: The pattern to search for.
    :return: True->Found.
    """
    # Check for valid search pattern
    if len(search_pattern) == 0:
        return False

    # Map wildcards to regex expressions
    for key, value in MapEscape.items():
        search_pattern = search_pattern.replace(key, value)

    # Start of string search
    if search_pattern[0] != '*':
        search_pattern = '^' + search_pattern

    # End of string search
    if search_pattern[-1] != '*':
        search_pattern += '$'

    # Map wildcards to regex expressions
    for key, value in MapRegEx.items():
        if key in search_pattern:
            search_pattern = search_pattern.replace(key, value)

    # Perform the search
    if re.search(search_pattern, source_string):
        return True
    else:
        return False


class EplStringSearch:
    """
    Class providing abstract string search functionality.
    """

    def __init__(self):
        pass