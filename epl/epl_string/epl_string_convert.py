def str_to_bool(v):
    """
    Converts string to bool.
    :param v: String holding the bool.
    :return: bool
    """
    # If already bool, return
    if isinstance(v, bool):
        return v

    if str(v).lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif str(v).lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise Exception('Boolean value expected.')
