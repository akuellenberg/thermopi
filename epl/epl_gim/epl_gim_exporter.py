from gim_exporter.gim import Node, GIM
from epl.epl_string.epl_string_split import split_always_list
import copy
from epl.epl_katcp.epl_katcp_gim import KatcpGimInfo
from datetime import datetime
import time
import httpx
import asyncio

KATCP_DELIMITER = "."
KATCP_REQ_DELIMITER = "-"
IGUI_DELIMITER = "::"
VALID_STATES = [1, 2, 3, 4, 5, 6]


class GenericGimExporter:
    """
    Class to provide a generic interface to the gim_exporter module.
    """

    def __init__(self,
                 gim_url,
                 gim_user,
                 gim_pass,
                 parent_device_name,
                 policies=None,
                 valid_istates=None,
                 override_gim=True,
                 logger=None,
                 sensor_min_update_rate_s=1800):
        """
        Init the module.
        :param gim_url: URL to connect with.
        :param gim_user: The gim user.
        :param gim_pass: GIM user's password.
        :param parent_device_name: The device's parent device name.
        :param policies: Task generation policies to be invoked when creating tasks.
        :param valid_istates: Sensor states to be supported (VALID_STATES if None)
        :param override_gim: True->Override existing tree/node configuration.
        :param logger: The epl_logging_log object to be used with logging.
        :param sensor_min_update_rate_s:
        """

        self._logger = logger
        self._gim = GIM(gim_url, gim_user, gim_pass)
        self._task_map = {}
        self._parent_device_name = parent_device_name
        self._valid_istates = VALID_STATES if valid_istates is None else valid_istates
        self._root_node = None
        self._override_gim = override_gim
        self._policies = policies
        self._max_update_period_s = sensor_min_update_rate_s

    async def init_connect(self):
        """
        Initially connect with gim. To be called once the exporter object was created and initialized.
        """

        # GIM authentication
        self._logger.write_info("Authenticating with gim-gui")
        await self._gim.authorize()

        # One way sync with the tree
        self._logger.write_info("Syncing gim tree for root node {}".format(self._parent_device_name))
        self._root_node = await self._gim.build_node_tree(parent=self._parent_device_name)

    async def shutdown(self):
        """
        Shutdown callback. Call on application exit.
        Will close connections and free resources.
        """
        self._logger.write_info("Shutting down exporter")
        await self._gim.shutdown()

    async def _autogen_task(self, sensor, params, config=None):
        """
        Autogeneration of a task from a sensor name.
        Currently, this only supports GET type
        tasks and does not do a corresponding request
        look up.
        :param sensor: Sensor name.
        :param params: Additional parameters defining the task.
        :param config:
        :return:
        """
        if config is None:
            opt_set = "get_set" if params["request"] else "get"
            config = {
                "options": opt_set,
                "metadata": {"mysql_task_type": opt_set.upper()}
            }

        if "units" in params and params["units"] != "discrete":
            config["metadata"]["unit"] = params["units"]
        if "description" in params:
            config["metadata"]["description"] = params["description"]
        if "upper_limit" in params:
            config["metadata"]["upper_limit"] = params["upper_limit"]
        if "lower_limit" in params:
            config["metadata"]["lower_limit"] = params["lower_limit"]
        if "value_type" in params:
            config["metadata"]["value_type"] = params["value_type"]

        self._logger.write_info(f"Autogenerating task {sensor} with config {config}")
        node = self._root_node.add_node(
            sensor, Node.TASKS,
            config, KATCP_DELIMITER)
        self._logger.write_debug(f"Added node {node}")

        # Here we can force an update to the sensor map
        self._root_node.sensor_lookup(sensor, refresh=True)

        # Sync the node to read back the id
        await node._sync_node(self._gim, update=self._override_gim)

        return node

    async def register_sensor(self, sensor, extra_params=None):
        """
        Registers a sensor with gim.
        :param sensor: Sensor name.
        :param extra_params:
        :return:
        """

        node = None

        self._logger.write_debug("Registering sensor {} with params {}".format(sensor, extra_params))

        try:
            node = self._root_node.sensor_lookup(sensor, refresh=True)
            self._logger.write_debug("Sensor already known")
        except KeyError:
            if "ignore_unknown" in self._policies:
                self._logger.write_debug("ignore_unknown policy invoked, ignoring sensor")
            elif "autogen_task" in self._policies:
                self._logger.write_debug("autogen_task policy invoked, generating sensor")
                node = await self._autogen_task(sensor, extra_params)

        return node

    async def sensor_bulk_update(self, sensor_data):
        """
        Perform a bulk update on sensors.
        :param sensor_data: {sensor_name: [timestamps],[data]}
        :return:
        """

        bulk_data = {}

        for name, data in sensor_data.items():
            try:
                node = self._root_node.sensor_lookup(name, refresh=False)

                # Only update in case of any new data
                for single_data in data:
                    if node.last_sensor_reading is None or \
                            single_data["current_value"] != node.last_sensor_reading["current_value"] or \
                            (float(single_data["timestamp"]) - float(node.last_sensor_reading["timestamp"])) > \
                            self._max_update_period_s:
                        bulk_data[node.node_id] = data
                        node.last_sensor_reading = data[-1]
                        break
                    else:
                        self._logger.write_debug(
                            "Sensor value same as last one, discarding update. sensor:{}, data:{}".format(
                                name, single_data["current_value"]))

            except KeyError:
                self._logger.write_warning(
                    "Received update for untracked sensor, adding same: {}".format(
                        name))
                # Registering new sensor
                node = await self.register_sensor(name, {
                    "value_type": data[0]["value_type"],
                    "request": ""
                })

                # Now that the sensor was registered, add data
                bulk_data[node.node_id] = data

        if len(bulk_data) > 0:
            # +++ Calculate bulk time
            start_bulk_update = datetime.now()

            # Execute the bulk request
            await self._gim.update_nodes(bulk_data)

            # --- Calculate bulk time
            dur_hist_call = datetime.now() - start_bulk_update

            self._logger.write_info("Duration for bulk update of {} entries: {}".
                                    format(len(bulk_data), dur_hist_call))
        else:
            self._logger.write_info("Empty data for the bulk, not executing request")

    async def sensor_update(self, sensor, value, timestamp, status):
        """
        Updates a sensor on gim. If the sensor does not exist, it will return False.
        :param sensor: The sensor name.
        :param value: The single value to be updated.
        :param timestamp: Timestamp in unix format.
        :param status: VALID_STATES
        :return: True->Update successful
        """
        self._logger.write_debug("Received update for sensor {} with reading {}, {}, {}".
                                 format(sensor, value, timestamp, status))
        if status not in self._valid_istates:
            self._logger.write_debug(
                "Handler ignoring reading with invalid istatus ({})".format(
                    status))
            return False
        try:
            node = self._root_node.sensor_lookup(sensor, refresh=False)
        except KeyError:
            self._logger.write_warning("Received update for untracked sensor: {}".format(sensor))
            return False
        else:
            self._logger.write_debug("Found associated node: {}".format(node))
            # Send update to IGUI
            try:
                await self._gim.update_node(
                    node.task_name(),
                    {"current_value": value,
                     "timestamp": timestamp,
                     "error_state": status
                     })
            except Exception as error:
                self._logger.write_exception("Exception while updating node: {}".format(
                    str(error)))
                return False
        return True

    async def update_from_tasklist(self, task_list):
        """
        Update the gim configuration from a task-list.
        :param task_list: {task_name:gim_config}
        :return:
        """
        gim_config = self.gim_config_create(task_list)
        self._root_node.update_from_config(gim_config)
        await self._root_node.sync_with_gim(
            self._gim, update=self._override_gim)

    def gim_config_create(self, task_list):
        """
        Sets up the gim config dict.
        :param task_list: {task_name:gim_config}
        :return:
        """

        # Dict to build the tree in
        gim_config_dict = dict()

        # Default sampling strategy and policies on root level
        gim_config_dict['default_sampling_strategy'] = {'type': 'event-rate', 'args': [5, 1800]}
        gim_config_dict['policies'] = ['ignore_unknown']

        # Iterate over task list and create the devices tree
        for task_name, gim_info in task_list.items():

            # Always start at root level
            cur_device = gim_config_dict

            # List with task names to build the tree
            task_name_list = list()

            # Add a virtual root name
            # if self._virtual_gim_roots is not None:
            #     task_name_list.append(self._virtual_gim_roots)

            # Split task name to get the different device levels
            task_name_list.extend(split_always_list(task_name, '.'))

            # Iterate over task name list to build the device tree
            for i in range(0, len(task_name_list) - 1):

                # The following 3 steps apply for all levels:
                # 1. Add devices entry
                # 2. Add the device
                # 3. Add global parameters

                # Device metadata
                device_metadata = dict()

                # Devices entry
                if 'devices' not in cur_device:
                    cur_device['devices'] = dict()

                # Check whether next device is already present (from another task)
                if task_name_list[i] not in cur_device['devices']:
                    cur_device['devices'][task_name_list[i]] = dict()

                # Store pointer to the next device
                cur_device = cur_device['devices'][task_name_list[i]]

                # +++ META
                # Set top-level device as icinga-host
                if i == 0:
                    device_metadata['icinga_host'] = '1'

                # Devices have to have GET type
                device_metadata['mysql_task_type'] = 'GET'

                # Set the device meta (need to do copy)
                cur_device['metadata'] = copy.deepcopy(device_metadata)
                # --- META

                # Global params on device level
                cur_device['sampling_strategy'] = {'type': 'event-rate', 'args': [5, 1800]}
                cur_device['policies'] = ['autogen_task']

            # Make sure level has tasks key (always needed to attach tasks)
            if 'tasks' not in cur_device:
                cur_device['tasks'] = dict()
            cur_device = cur_device['tasks']

            if gim_info is not None:

                # Some widget types need special treatment
                drop_down_options = dict()
                if gim_info.mysql_task_type is KatcpGimInfo.WidgetType.DROPDOWN:
                    if gim_info.limits is not None:
                        for limit in gim_info.limits:
                            drop_down_options[limit] = str(limit)

                # Add task
                cur_device[task_name_list[-1]] = {  # Use task name as key
                    'sensor_name': '',
                    'request_name': '',
                    'options': gim_info.options_available.name.lower() \
                        if gim_info.options_available is not None else '',
                    'metadata':
                        {
                            'display_name': gim_info.display_name,
                            'lower_limit': gim_info.limits[0]
                            if gim_info.limits is not None else '',
                            'upper_limit': gim_info.limits[-1]
                            if gim_info.limits is not None else '',
                            'value_type': gim_info.value_type,
                            'sampling_strategy':
                                {'type': gim_info.sampling_strategy.value,
                                 'args': gim_info.sampling_strategy_params} \
                                    if gim_info.sampling_strategy is not None \
                                    else {'type': 'event-rate', 'args': [5, 1800]},
                            'mysql_task_type': gim_info.mysql_task_type.name \
                                if gim_info.mysql_task_type is not None else '',
                            'description': gim_info.description \
                                if gim_info.description is not None else '',
                            'options': drop_down_options,
                            'option01': gim_info.option01 if gim_info.option01 is not None else '',
                            'option02': gim_info.option02 if gim_info.option02 is not None else '',
                            'option03': gim_info.option03 if gim_info.option03 is not None else '',
                            'option04': gim_info.option04 if gim_info.option04 is not None else '',
                            'unit': gim_info.unit if gim_info.unit is not None else '',
                            'format': gim_info.format if gim_info.format is not None else '',
                            'icinga_check_constant_errors': gim_info.icinga_check_constant_errors \
                                if gim_info.icinga_check_constant_errors is not None else '',
                        }
                }
        return gim_config_dict

    async def heartbeat(self):
        """
        Callback to implement the heartbeat functionality.
        Should be executed as loop: loop.create_task(exporter.heartbeat())
        """

        def task_selector(_task):
            if _task.parent is None:
                return False
            else:
                return all([
                    _task.node_name == "connected",
                    _task.parent.node_name == "sidecar"
                ])

        updates = {}

        # Wait before starting the heartbeats, need to sync with gim first
        await asyncio.sleep(300)

        while True:
            timestamp = "{:.6f}".format(time.time())
            for task in self._root_node.list_tasks(filter_func=task_selector):
                self._logger.write_info("Heartbeat on task {}".format(task))

                if task is not None:
                    if task.node_id is not None:
                        updates[task.node_id] = [
                            {
                                "current_value": 1,
                                "timestamp": timestamp,
                                "error_state": "1",
                                "value_type": "int"
                            }
                        ]
                    else:
                        self._logger.write_warning("node-id is none for node, {}".format(task))
                else:
                    self._logger.write_warning("None-node found with heartbeat-tasks")

            try:
                await self._gim.update_nodes(updates)
            except httpx.ConnectError as error:
                self._logger.write_error("No connection to gim, heartbeat failed")
            except Exception as error:
                self._logger.write_error("Error on heartbeat update: {}".format(str(error)))
            finally:
                await asyncio.sleep(60)
