from datetime import datetime, timezone, timedelta


def dt_now_tz(tz=timezone.utc, offset=timedelta(days=0, hours=0, minutes=0)):
    """
    Returns the datetime.now with respect to the provided timezone.
    :param tz: datetime.timezone.
    :param offset: datetime.timedelta to be added (subtracted when negative).
    :return: datetime
    """

    # Get current time as utc
    dt = datetime.now(tz) + offset
    return dt.replace(tzinfo=tz)


def dt_now_tz_timestamp(tz=timezone.utc, offset=timedelta(days=0, hours=0, minutes=0)):
    """
    Returns the datetime.now with respect to the provided timezone as timestamp.
    :param tz: datetime.timezone.
    :param offset: datetime.timedelta to be added (substracted when negative).
    :return: datetime
    """

    # Current time as timestamp
    return dt_now_tz(tz=tz, offset=offset).timestamp()
