from epl.epl_katcp.epl_katcp_client import KatcpClient
from epl.epl_katcp.epl_katcp_server import KatcpServer, ServerTask
from epl.epl_katcp.epl_katcp_sensor import KatcpSensor, Sensor
from epl.epl_katcp.epl_katcp_request import KatcpRequest
from epl.epl_katcp.epl_katcp_command import CommandState
from epl.epl_katcp.epl_katcp_gim import KatcpGimInfo
from epl.epl_string.epl_string_split import split_always_list
from epl.epl_string.epl_string_search import wildcard_search
from copy import deepcopy
import time
import signal
from enum import Enum, auto


class KatcpServerWrapper:
    """
    Main class for the KatcpServerWrapper
    """

    class WrapperRootUsage(Enum):
        """
        Enum to define the usage of the wrapper_root argument
        """
        NONE = auto()  # Don't use at all
        GIM_CONFIG = auto()  # Use with gim-config
        WRAPPER_PREFIX = auto()  # Use as root-prefix for wrapper sensors/requests
        DISPLAY = auto()  # Use for displaying purposes

    # Requests to be ignored (mostly because they are added by the wrapper-katcp-server again)
    _REQ_IGNORE_MAP = ['arb', 'client-list', 'define', 'dict', 'dispatch', 'get', 'halt', 'help', 'job',
                       'listener-create', 'log-default', 'log-level', 'log-limit', 'log-record', 'notice',
                       'process',
                       'restart', 'search', 'sensor', 'sensor-limit', 'sensor-list', 'sensor-sampling',
                       'sensor-sampling-clear', 'sensor-value', 'set', 'spectrometer', 'system-info',
                       'version',
                       'version-list', 'watchdog']

    # Dict to map client to server sensors
    _WRAPPER_SENSOR_MAP = {}

    # Dict to map generic client to server requests
    _WRAPPER_REQ_MAP = {}

    def __init__(self, server_host, server_port, server_sensor_update_rate, wrapper_host, wrapper_port, server_root,
                 wrapper_root, wrapper_root_usage, command_white_list, gim_info_definitions, wrapper_sampling_strategy,
                 wrapper_sampling_strategy_args, prevent_device_name_shadowing, log_level, logger_main, logger_katcp):
        """
        Inits the wrapper object.
        :param server_host: Host of server-katcp-server to be wrapped.
        :param server_port: Port of server-katcp-server to be wrapped.
        :param server_sensor_update_rate: Update-rate for server sensors (period-based sampling only).
        :param wrapper_host: Host of wrapper-katcp-server to listen on.
        :param wrapper_port: Port of wrapper-katcp-server to listen on.
        :param server_root: Root name of server (used with spectra snapshot).
        :param wrapper_root: Root name to prefix all the wrapper sensors/requests with.
        :param wrapper_root_usage: Defines the usage of the wrapper_root prefix.
        :param command_white_list: List with sensor/request names to be wrapped (empty->add-all, others ignored).
        :param gim_info_definitions: Dict with predefined gim info.
        :param wrapper_sampling_strategy: Sampling strategy to be used for gim-config with the katcp-server.
        :param wrapper_sampling_strategy_args: Arguments to be used with the server sampling strategy.
        :param prevent_device_name_shadowing: True->Ignore sensors/requests that shadow device names (issue with gim)
        :param log_level: log_level
        :param logger_main: Main application logger.
        :param logger_katcp: Katcp module logger.
        """

        # General variables
        self._server_host = server_host
        self._server_port = server_port
        self._server_sensor_update_rate = server_sensor_update_rate
        self._wrapper_host = wrapper_host
        self._wrapper_port = wrapper_port
        self._server_root = server_root
        self._wrapper_root = wrapper_root
        self._wrapper_root_usage = wrapper_root_usage
        self._white_list = command_white_list if command_white_list is not None else []
        self._log_level = log_level
        self._logger_main = logger_main
        self._logger_katcp = logger_katcp
        self._wrapper_sampling_strategy = wrapper_sampling_strategy
        self._wrapper_sampling_strategy_args = wrapper_sampling_strategy_args
        self._main_thread_stop = False
        self._katcp_server = None
        self._prevent_device_name_shadowing = prevent_device_name_shadowing

        # Virtual root name for katcp servers
        if self._wrapper_root != '' \
                and KatcpServerWrapper.WrapperRootUsage.GIM_CONFIG in self._wrapper_root_usage \
                and KatcpServerWrapper.WrapperRootUsage.WRAPPER_PREFIX not in self._wrapper_root_usage:
            # Only add if not already added with sensor/request names (otherwise doubled)
            katcp_virtual_root = self._wrapper_root
        else:
            katcp_virtual_root = None

        # Predefined gim info definitions
        if gim_info_definitions is not None:

            # Copy them to not change the original data
            self._gim_info_definitions = deepcopy(gim_info_definitions)

            # Wrap the keys (pop original entries)
            for sensor_name in self._gim_info_definitions.keys():
                self._gim_info_definitions[
                    self._wrap_sens_req_name(sensor_name=sensor_name, purpose=self._wrapper_root_usage)] = \
                    self._gim_info_definitions.pop(sensor_name)
        else:
            self._gim_info_definitions = {}

        # Specific message handles
        # Needs to be defined here since the handles need the `self` object
        self._MESSAGE_HANDLES = {
            'sensor-status':
                (self._handle_sensor_status_reply, self._handle_sensor_status_inform),
        }

        # Dict to map special server requests: not simply ones to be forwarded to client
        # Needs to be defined here since the handles need the `self` object
        # {req_name:[KatcpCommand, special_callback]}
        self._WRAPPER_REQ_SPECIAL_MAP = {}

        # Connect exit handler to properly stop the application on ctrl+c
        signal.signal(signal.SIGINT, self.on_exit)

        # Init a katcp client to connect the server
        self._katcp_client = KatcpClient(device_host=self._server_host,
                                         device_port=self._server_port,
                                         device_name='wrapper',
                                         katcp_root_name=self._server_root,
                                         connect_timeout=5,
                                         init_timeout=float(5),
                                         update_sensors_dict=dict(),
                                         command_list=dict(),
                                         auto_reconnect=True,
                                         sensor_period_default=self._server_sensor_update_rate,
                                         logger=self._logger_katcp,
                                         message_handles=self._MESSAGE_HANDLES,
                                         description_cleanup_handle=self._description_clean_up)

        # Endless loop to connect client and init server
        while self._main_thread_stop is False:

            # If uninitialized, initialize
            if self._katcp_client.device_state_get(keep=True) != KatcpClient.DeviceStates.INITIALIZED:

                # If server was already running, stop it to get it re-initialized
                if self._katcp_server is not None:
                    self._katcp_server.stop()

                # Wait for client to be initialized
                while self._katcp_client.device_state_get(keep=True) != KatcpClient.DeviceStates.INITIALIZED:

                    # Make sure to stop also during init
                    if self._main_thread_stop:
                        return

                    # Init the client
                    self._logger_main.write_info('Initializing client {}:{}.'.format(self._server_host, self._server_port))
                    self._katcp_client.init_connect()

                    # If not successful, be patient...
                    time.sleep(1)

                # Init the katcp server
                self._katcp_server = KatcpServer(host=self._wrapper_host, port=self._wrapper_port,
                                                 tasks=self._create_katcp_tasks(self._katcp_client),
                                                 callback_function=self._request_callback, logger=self._logger_katcp,
                                                 virtual_gim_roots=katcp_virtual_root,
                                                 sampling_strategy_default=self._wrapper_sampling_strategy,
                                                 sampling_strategy_default_args=self._wrapper_sampling_strategy_args)

                # Start the server
                self._katcp_server.start()
                self._logger_main.write_info('Starting katcp server {}:{}.'.format(self._wrapper_host, self._wrapper_port))

            time.sleep(1)

    def _request_callback(self, msg):
        """
        Generic callback to forward requests to the client.
        :param msg: The request message.
        :return: 'state','message'
        """

        if msg.name in self._WRAPPER_REQ_SPECIAL_MAP:
            command = self._WRAPPER_REQ_SPECIAL_MAP[msg.name][0]
            self._WRAPPER_REQ_SPECIAL_MAP[msg.name][1](command)
        else:
            # Get related command
            command = self._WRAPPER_REQ_MAP[msg.name]
            command.command_set(command_state=CommandState.INIT)

            # Create request
            send_message = '?{}'.format(command.command_name)
            if len(msg.arguments) > 0:
                send_message = '{} {}'.format(send_message, ' '.join([x.decode() for x in msg.arguments]))

            # Send
            self._katcp_client.send_request(send_message)

            # Wait for the server's response
            wait_response = 0
            while 1:
                if command.command_state != CommandState.INIT:
                    break
                elif wait_response > 300:
                    command.command_set(CommandState.FAIL, reply_message='Timeout, no answer from server')
                wait_response += 1
                time.sleep(0.01)

            # In case of != OK, at least some info for the client
            if command.command_state != CommandState.OK and command.reply_message == '':
                command.command_set(command.command_state, reply_message='Server did not provide any info?!')

        return {
            'status': command.command_state.name.lower(),
            'reply': command.reply_message,
            'inform': command.inform_message
        }

    def _wrap_sens_req_name(self, sensor_name=None, request_name=None, purpose=WrapperRootUsage.NONE):
        """
        Returns the wrapper-sensor name for a given server_sensor_name.
        :param sensor_name: Name of server sensor.
        :param request_name: Name of server request.
        :param purpose: WrapperRootUsage enum to define the purpose of the wrapped sensor_name.
        :return: Wrapped name, when purpose in self._wrapper_root_usage. Returns None in case no names are passed.
        """
        # Check whether purpose must be wrapped
        if purpose in self._wrapper_root_usage and purpose is not self.WrapperRootUsage.NONE:
            # Only if we have a wrapper_root prefix
            if self._wrapper_root != '':
                # Distinguish sensor/request
                if sensor_name is not None:
                    return '{}.{}'.format(self._wrapper_root, sensor_name)
                elif request_name is not None:
                    return '{}-{}'.format(self._wrapper_root, request_name)

        # Else simply return the original name
        return sensor_name if sensor_name is not None else request_name

    def _handle_sensor_status_inform(self, katcp_client, message):
        """
        Specific handler for the sensor-status inform.
        :param katcp_client: The KatcpClient object for that message.
        :param message: Entire katcp message
        :return:
        """

        # Convert katcp message object to string
        message_string = str(message)

        # Split message fields
        fields = message_string.split()

        # Get sensor
        try:
            katcp_sensor = self._WRAPPER_SENSOR_MAP[fields[3]]

            # Update sensor
            katcp_sensor.set_value(katcp_sensor.TYPE_CASTINGS[katcp_sensor.type](fields[5]),
                                   KatcpSensor.SensorStatus
                                   [fields[4]],
                                   fields[1])
        except:
            self._logger_main.write_debug('_handle_sensor_status_inform: Sensor {} not in wrapper sensor list'.
                                          format(fields[3]))

    def _handle_sensor_status_reply(self, katcp_client, message):
        """
        Specific handler for the sensor-status inform.
        :param katcp_client: The KatcpClient object for that message.
        :param message: Entire katcp message
        :return:
        """
        pass

    def _description_clean_up(self, help_inform_message):
        """
        Clean up the help messages.
        :param help_inform_message: help inform message.
        :return:
        """

        # Decode description
        description_string = help_inform_message.arguments[1].decode()

        # Do clean-up
        # eg. description_string = description_string.replace('||', '|')

        # Encode description again
        help_inform_message.arguments[1] = description_string.encode()

        return help_inform_message

    def _create_katcp_tasks(self, katcp_client):
        """
        Creates KATCP sensors/requests from the katcp_client's task-list and stores them
        in a list that can be passed through the server to be registered there.
        :param katcp_client: katcp_client to copy sensors/requests from.
        :return: None
        """

        def _device_shadowing_check(shadowing_sensor_name):
            """
            Check whether a given sensor name shadows an existing device name in the task_list.
            Note: No manipulation will be performed on the sensor/device-name.
            :param shadowing_sensor_name: The sensor-name to look for in the task_list.
            :return: True->Ok, sensor can be added; False->Sensor shadows and must be dropped.
            """

            # Prevent device name shadowing
            for shadowing_task_name in all_tasks.keys():

                # Get lengths
                _shadowing_sensor_name_len = len(shadowing_sensor_name.split('.'))
                _shadowing_task_name_len = len(shadowing_task_name.split('.'))

                # A two-way search must be performed. Since we are looking for device-names, entry
                # with more hierarchy levels must be stripped by last one.
                if _shadowing_sensor_name_len > _shadowing_task_name_len:
                    if shadowing_sensor_name.rsplit('.', 1)[0] == shadowing_task_name:
                        return False
                elif _shadowing_task_name_len > _shadowing_sensor_name_len:
                    if shadowing_task_name.rsplit('.', 1)[0] == shadowing_sensor_name:
                        return False

            # No shadowing found
            return True

        def _white_list_check(white_list_sensor_name):
            """
            Check sensor name against white_list.
            :param white_list_sensor_name: The sensor same to check for.
            :return: True->Found in white_list or white_list empty.
            """
            # Helper for return value
            ret_val = False

            # Empty white_list?
            if len(self._white_list) == 0:
                return True

            # Iterate over white_list and perform the search
            for entry in self._white_list:
                if wildcard_search(white_list_sensor_name, entry) is True:
                    ret_val = True

            return ret_val

        all_tasks = dict()

        # 1. Add sensor/request pairs based on sensors
        for sensor_name, sensor in katcp_client.SENSOR_MAP.items():

            # Check whether sensor is in white list
            if _white_list_check(sensor_name) is False:
                continue

            # Convert sensor names to wrapper names
            sensor.name = self._wrap_sens_req_name(sensor_name=sensor_name,
                                                   purpose=self.WrapperRootUsage.WRAPPER_PREFIX)

            # Prevent device name shadowing ?
            if self._prevent_device_name_shadowing is True:
                if _device_shadowing_check(sensor.name) is False:
                    self._logger_main.write_warning('{} ignored to prevent device name shadowing'.
                                                    format(sensor.name))
                    continue

            # New task to be added
            new_task = ServerTask()

            # Add sensor
            new_task.katcp_sensor = deepcopy(sensor)

            # Check for corresponding request
            try:
                # Try to find the command
                command = katcp_client.COMMAND_MAP[sensor_name.replace('.', '-')]

                # Ignore certain requests
                if command.command_name not in self._REQ_IGNORE_MAP:
                    # Create request
                    new_task.katcp_request = KatcpRequest(command.command_type, command.command_name,
                                                          command.description, '', command.values)
                    # Add link to the wrapper_map
                    self._WRAPPER_REQ_MAP[sensor.name.replace('.', '-')] = command
            except:
                pass

            # Add new task to tasks dict (to be added to server)
            all_tasks[sensor.name] = new_task

            # Add link to the wrapper_map
            self._WRAPPER_SENSOR_MAP[sensor_name] = new_task.katcp_sensor

        # 2. Add standalone requests
        for command_name, command in katcp_client.COMMAND_MAP.items():

            # Check whether command is in white list
            if _white_list_check(command_name) is False:
                continue

            # Ignore certain requests
            if command_name in self._REQ_IGNORE_MAP:
                continue

            # Convert command names to wrapper names
            command_name_add = self._wrap_sens_req_name(request_name=command.command_name,
                                                        purpose=self.WrapperRootUsage.WRAPPER_PREFIX)

            # Create the command name for the dict lookup
            command_name_search = command_name_add.replace('-', '.')

            # Check whether command is already in task list
            command_present = False
            for search_task in all_tasks.keys():
                if command_name_search == search_task.replace('-', '.'):
                    command_present = True
                    break

            if command_present is True:
                continue

            # Prevent device name shadowing?
            if self._prevent_device_name_shadowing is True:
                if _device_shadowing_check(command_name_search) is False:
                    self._logger_main.write_warning('{} ignored to prevent device name shadowing'.
                                                    format(command_name_search))
                    continue

            # New task to be added
            new_task = ServerTask()

            # Create request
            new_task.katcp_request = KatcpRequest(command.command_type, command_name_add,
                                                  command.description, '', command.values)

            # Add new task to tasks dict
            all_tasks[command_name_search] = new_task

            # Add link to the wrapper_map
            self._WRAPPER_REQ_MAP[command_name_add] = command

        # 3. gim-info
        for task_name_search, task in all_tasks.items():

            # Check whether task has a predefined gim-config definition
            if task_name_search in self._gim_info_definitions:
                task.gim_info = self._gim_info_definitions[task_name_search]
            else:
                if task.katcp_sensor is not None:
                    if Sensor.parse_type(task.katcp_sensor.type) != Sensor.DISCRETE:
                        value_type = task.katcp_sensor.type
                    else:
                        value_type = 'str'
                    if len(task.katcp_sensor.params) > 0:
                        gim_limits = task.katcp_sensor.params
                    else:
                        gim_limits = None
                else:
                    if len(task.katcp_request.params) > 0:
                        gim_limits = task.katcp_request.params
                    else:
                        gim_limits = None
                    value_type = task.katcp_request.type

                if task.katcp_request is None:
                    mysql_task_type = KatcpGimInfo.WidgetType.GET
                    options_available = KatcpGimInfo.OptionsAvailable.GET
                elif task.katcp_sensor is None:
                    mysql_task_type = KatcpGimInfo.WidgetType.SET
                    options_available = KatcpGimInfo.OptionsAvailable.SET
                else:
                    if Sensor.parse_type(task.katcp_sensor.type) == Sensor.DISCRETE:
                        mysql_task_type = KatcpGimInfo.WidgetType.DROPDOWN
                    elif Sensor.parse_type(task.katcp_sensor.type) == Sensor.BOOLEAN:
                        mysql_task_type = KatcpGimInfo.WidgetType.BOOL
                    else:
                        mysql_task_type = KatcpGimInfo.WidgetType.GET_SET
                    options_available = KatcpGimInfo.OptionsAvailable.GET_SET

                task.gim_info = KatcpGimInfo(logger=self._logger_main, value_type=value_type,
                                             mysql_task_type=mysql_task_type,
                                             sampling_strategy=self._wrapper_sampling_strategy
                                             if task.katcp_sensor is not None else None,
                                             sampling_strategy_params=self._wrapper_sampling_strategy_args
                                             if task.katcp_sensor is not None else None,
                                             description=task.katcp_sensor.description if task.katcp_sensor is not None
                                             else task.katcp_request.description, options_available=options_available,
                                             display_name=split_always_list(task_name_search, '.')[-1],
                                             limits=gim_limits)

        # Special requests
        for req_name, req_details in self._WRAPPER_REQ_SPECIAL_MAP.items():
            # KatcpCommand
            new_command = req_details[0]

            new_task = ServerTask()

            # Create request
            new_task.katcp_request = KatcpRequest(new_command.command_type, new_command.command_name,
                                                  new_command.description, '', new_command.values)

            # Add new task to tasks dict
            all_tasks[req_name] = new_task

        return all_tasks

    def on_exit(self, signum, frame):
        """
        Stops all related threads and modules.
        :return:
        """
        # Stop client and server
        self._katcp_client.stop()
        self._katcp_server.stop()
        self._main_thread_stop = True
