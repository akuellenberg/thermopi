from enum import Enum


class CommandType(Enum):
    """
    Enum with command types for katcp commands.
    """
    UNKNOWN = 0
    INT = 1
    FLOAT = 2
    DISCRETE = 3
    STRING = 4


class CommandState(Enum):
    """
    Enum with command states for katcp commands.
    """
    UNKNOWN = 0
    OK = 1
    FAIL = 2
    INVALID = 3
    INIT = 4


class KatcpCommand:
    """
    Class providing katcp command functionality.
    """
    def __init__(self,
                 command_name,
                 command_type=CommandType.UNKNOWN,
                 command_state_init=CommandState.UNKNOWN,
                 values=list(),
                 description=''):
        """
        Init the command.
        :param command_name: Command name.
        :param command_type: CommandType.
        """

        # Set variables
        self.command_name = command_name
        self.command_type = command_type
        self.command_state = command_state_init
        self.inform_message = ''
        self.reply_message = ''
        self.values = values
        self.description = description

        self.was_updated = False

    def command_set(self, command_state='', inform_message=list(), reply_message=list()):
        """
        Sets the commands state.
        :param command_state: CommandState.
        :param inform_message: Inform message.
        :param reply_message: Reply message.
        :return:
        """
        try:
            # Support for enum and simple string state
            if command_state != '':
                if isinstance(command_state, CommandState) is True:
                    self.command_state = command_state
                else:
                    self.command_state = CommandState[command_state.upper()]
                # Init cleans the messages
                if command_state == CommandState.INIT:
                    self.inform_message = []
                    self.reply_message = b''

            # The inform message
            if len(inform_message) > 0:
                self.inform_message.append(inform_message)

            # The reply message
            if len(reply_message) > 0:
                self.reply_message = b' '.join(reply_message)
        except:
            self.command_state = CommandState.UNKNOWN

    def send_request(self):
        pass
