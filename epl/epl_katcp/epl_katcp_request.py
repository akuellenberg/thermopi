
class KatcpRequest:
    """
    Class to handle info on Katcp requests.
    """
    def __init__(self, request_type, name, description=None, units='', limit_params=None):
        """
        Inits the object.
        :param request_type: Type of request.
        :param name: Request's full name `x-y-z`
        :param description: A description for the help output.
        :param units: Currently not used.
        :param limit_params: Limits, currently only used for the help output.
        """
        if limit_params is None:
            limit_params = list()
        self.name = name
        self.type = request_type
        self.description = description
        self.params = limit_params
        self.units = units
