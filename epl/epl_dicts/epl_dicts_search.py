def search_nearest(search_dict, search_value):
    """
    Search the nearest key of search_value in search_dict, returning the found key.
    :param search_dict: Dict to search in.
    :param search_value: Value to search for.
    :return: key of the nearest entry.
    """
    return min(search_dict, key=lambda x: abs(x - search_value))


def sub_string_in_keys(search_dict, search_string):
    """
    Returns the first value where key contains a substring.
    :param search_dict: Dict to be searched in.
    :param search_string: Search string to look for as substring.
    :return: Found->Value; Not found->None.
    """
    return any(search_string in key for key in search_dict)
