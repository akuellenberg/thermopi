#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 27 11:17:10 2019

@author: hardikar
"""

import can

class CanHardwareSetup():
    
    def __init__(self, can_interface, filters):
        """
        This function, based on the parameter values passed, initializes the can hardware interface, i.e, bus. This
        values can then be used in the canicom library for data transfer.

        :param can_interface: can_interface as a string - e.g.,'can0' or 'can1', as used by the underlying hardware.
        :param filters: Values of the hardware addresses, which depend upon the devices which will be using the CAN
            interface. These addresses should be in the 29-bit extended format.
        :return:

        """
        super(CanHardwareSetup, self).__init__()
        self.can_interface = can_interface
        
        filter_dict = {}
        self.can_filters = []
        for i in range(len(filters)):
            filter_dict = {"can_id": filters[i][0], "can_mask": filters[i][1]}
            self.can_filters.append(filter_dict)
        
        self.bus = can.interface.Bus(self.can_interface, 
                                     bustype='socketcan_native', 
                                     can_filters=self.can_filters)
        
