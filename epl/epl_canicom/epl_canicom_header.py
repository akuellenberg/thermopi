#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 26 08:09:47 2019

@author: pi
"""
from enum import Enum

# Values
mirror_timeout_flag = False
mirror_timeout_flag_old = False
timer_enable_flag = True

# CANICOM_SENDER_ID = 0x180
CANICOM_MESSAGE_SINGLE_DATA_COUNT = 8
CANICOM_PACKETS_MAX_COUNT = 16
CANICOM_MESSAGE_PACKETS_DATA_COUNT = (CANICOM_MESSAGE_SINGLE_DATA_COUNT*CANICOM_PACKETS_MAX_COUNT)
CANICOM_COMMANDS_MAX_COUNT = 500   # Max number of commands to handle in the send/receive queues
CANICOM_COMMANDS_PENDING_MAX_COUNT = 32  # Max number of commands to handle in the pending queues
CANICOM_COMMAND_MIRROR_MAX_PENDING = 50  # Max time to wait for a mirror (* 100ms)
CANICOM_COMMAND_RX_MAX_PENDING = 10
CANICOM_COMMAND_SEND_MAX_PENDING = 50  # Max time to wait to finish a send process (* 100ms)
CANICOM_WAKEUP_COUNTER_INIT_VAL = 1  # Time to wait after a wake up command (multiples of 5ms)

# Global Error-Messages
CANICOM_CMDERROR_NOERROR = 0x00
CANICOM_CMDERROR_CMD_NOT_KNOWN = 0x01
CANICOM_CMDERROR_TYPE_NOT_AVAIL = 0x02
CANICOM_CMDERROR_VAL_HIGH = 0x03
CANICOM_CMDERROR_VAL_LOW = 0x04
CANICOM_CMDERROR_MIRROR_TIMEOUT = 0x05

CANICOM_LIBRARY_VERSION = "01.01.01"
CANICOM_LIBRARY_VERSION_LENGTH = (len(CANICOM_LIBRARY_VERSION))

CANICOM_TASKS_MAX_COUNT = 20
CANICOM_TASKS_COMMAND_VALUESIZE = 4


class TasksRequestCode(Enum):
    """
    Tasks request codes
    """
    INTRODUCE = 0x01
    BOOTLOADER = 0x03
    
    
class ReqType(Enum):
    """
    Enum to store various Request types of CAN messages.
    """
    
    REQTYPE_ERROR = '0x0',
    REQTYPE_SET = '0x1',
    REQTYPE_GET = '0x2',
    REQTYPE_ANSWER = '0x3',
    REQTYPE_MIRROR_ASYNC = '0x4',
    REQTYPE_MIRROR_SYNC = '0x5'


class MessageError(Enum):
    """
    Enum to store various error codes.
    """
    
    NOERROR = '0x00'
    CMDNOTKNOWN = '0x01'
    OPTNOTAVAIL = '0x02'
    VALUE_TOO_HIGH = '0x03'
    VALUE_TOO_LOW = '0x04'
    MIRROR_TIMEOUT = '0x05'
    RECEIVE_TIMEOUT = '0x06'
    PTYPE_UNKNOWN = '0x07'


class MessagePackageType(Enum):
    """
    Enum to store the packet types of the message.
    """
    
    HEADER = 0
    DATA = 1


class CanId:
    """
    Class for describing various fields of the 29-bit can Id.
    """
    
    def __init__(self, tx_id, rx_id, req_type, pack_type):

        self.sender_id = tx_id  
        self.recipient_id = rx_id
        self.req_type = req_type
        self.pack_type = pack_type

        self.group_id = 0
        self.device_id = 0


class MessageHeader:
    """
    Class for describing various fields of the 8-byte canicom messsage header.
    """
    
    def __init__(self, cnt_packages, err_code, cmd, cmd_spec_val):
        
        self.count_packages = cnt_packages
        self.error_code = err_code
        self.command = cmd
        self.command_spec_value = cmd_spec_val   


class MessageData:
    """
    Class for describing the data field of the canicom command.
    """
    
    def __init__(self, data_val):        
        self.data_val = data_val


class Command:
    """
    Class for describing various attributes of the canicom command.
    """
    
    def __init__(self, can_id, header, pending_counter, rx_tx_counter, decimal_data=[]):
        """

        :param can_id: Can id of the frame, made of tx address, rx address, req type and pack type.
        :param header: Header of the CAN command.
        :param pending_counter: Number of commands left to be received.
        :param rx_tx_counter: Number of transmitted/received command frames.
        :param decimal_data: Data to be sent with the command.
        :return:
        """

        self.can_id = can_id
        self.header_val = header
        self.pending_counter = pending_counter  # Counter be used to provide timeout functionality
        self.already_sent_received_counter = rx_tx_counter  # Number of packages that have already been sent/received
        self.data = decimal_data
        self.data_set = []


class TasksRequestCode(Enum):
    """
    Enum to store various Task Request Codes of CAN messages.
    """
    
    INTRODUCE = 0x01
    RESET = 0x02
    BOOTLOADER = 0x03
    CAN_ID = 0xFF


class TasksOptions(Enum):
    """
    Enum for storing available ParmTasks options.
    """
    
    NONE = 0
    GET = 1
    SET = 2
    GET_SET = 3
    MIRROR = 4


class TasksPType(Enum):  
    """
    Enum to store available parameter types.
    """
    
    NONE = 0
    FLOAT = 1
    BYTEARRAY = 2
    I32 = 3   
