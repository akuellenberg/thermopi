#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 10 08:20:14 2019

@author: hardikar
"""
from epl.epl_canicom import epl_canicom_header
import threading
import collections
from epl.epl_canicom import epl_canicom_data_extraction as dm


lock = threading.Lock()

CANICOM_TX_UPDATE_COMMANDS_MAX_COUNT = 1000
CANICOM_TX_SET_COMMANDS_MAX_COUNT = 50

tx_buffer = collections.deque(maxlen=1000)
tx_set_buffer = collections.deque(maxlen=50)


def add_in_send_buffer(cmd):
    """
    This function adds the command at the end of send buffer if it's not full.
    
    :param cmd: Canicom command.
        
    :return:
    """
    if len(tx_buffer) < CANICOM_TX_UPDATE_COMMANDS_MAX_COUNT:
        tx_buffer.append(cmd)
    else:
        print('transmit queue for update commands full (due to a command added from the RHS)', len(tx_buffer))
        tx_buffer.clear()
        tx_buffer.append(cmd)


def add_on_top(cmd):
    """ 
    This function puts the command on the top of the sending buffer,so that 
    they ultimately have the higher priority for sending. 
    
    :param cmd: canicom command.
    
    :return:
    """
    
    if len(tx_buffer) < CANICOM_TX_SET_COMMANDS_MAX_COUNT:
        tx_buffer.insert(0, cmd)
    else:
        tx_buffer.clear()
        print('transmit queue for update commands full (due to command added from LHS)')
        tx_buffer.insert(0, cmd)
