from fabric import ThreadingGroup, exceptions
from enum import Enum, auto


class SystemStatsRemoteLinux:
    class ExceptionRunCommand(Exception):
        """
        Exception indicating some general error with a command executed.
        """
        pass

    class ProcessListColumns(Enum):
        """
        Enum to define process list columns.
        """

        COMMAND = "command"
        CPU = "%cpu"
        MEM = "%mem"

    class RetValState(Enum):
        """
        Enum to define run command return values.
        """

        OK = auto()
        FAIL = auto()

    class RetVal:
        """
        Class defining a common struct for return values.
        """

        def __init__(self, host, status, output):
            """
            Init.
            :param host: Host string.
            :param status: RetValState
            :param output: Output string.
            """
            self.host = host
            self.status = status
            self.output = output

    class TopOutput:

        def __init__(self, pid, user, pr, ni, virt, res, shr, s, cpu, mem, time, command):
            """
            Init the top output object. The variables represent 1:1 the top columns.
            :param pid:
            :param user:
            :param pr:
            :param ni:
            :param virt:
            :param res:
            :param shr:
            :param s:
            :param cpu:
            :param mem:
            :param time:
            :param command:
            """
            self.pid = pid
            self.user = user
            self.pr = pr
            self.ni = ni
            self.virt = virt
            self.res = res
            self.shr = shr
            self.s = s
            self.cpu = cpu
            self.mem = mem
            self.time = time
            self.command = command

    class UptimeOutput:

        def __init__(self, uptime_output):
            """
            Class to handle fields from uptime output:
            :param uptime_output: str containing the content from /proc/uptime + /proc/loadavg space separated.
            """
            output_list = uptime_output.split(" ")
            self.uptime = float(output_list[0]) / 86400
            self.load_1min = float(output_list[-5])
            self.load_5min = float(output_list[-4])
            self.load_15min = float(output_list[-3])

    def __init__(self, logger, hosts, user, persistent_connections=True):
        """
        Initialize the module.
        :param logger: epl_logging.log.Log object.
        :param hosts: List with hosts to be queried (str).
        :param user: Username to be used for hosts connection.
        :param persistent_connections: True->Persistent connections to hosts.
        """

        self._logger = logger
        self._hosts = hosts
        self._user = user
        self._connect_kwargs_timeout = 20
        self._connect_kwargs_banner_timeout = 20
        self._connect_kwargs_auth_timeout = 20
        self._connection_keep_open = persistent_connections

        if self._connection_keep_open:
            self._t_group = ThreadingGroup(*self._hosts,
                                           user=self._user,
                                           connect_kwargs={
                                               "timeout": self._connect_kwargs_timeout,
                                               "banner_timeout": self._connect_kwargs_banner_timeout,
                                               "auth_timeout": self._connect_kwargs_auth_timeout
                                           })

    def _host_string_create(self, connection):
        """
        Creates a host string based on connection info.
        :param connection: The connection object.
        """
        return "{}:{}".format(connection[0].host, connection[0].port) \
            if connection[0].port != 22 else connection[0].host

    def _result_handle_failed(self, result_failed, ret_list):
        """
        Implements a generic way to handle failed results.
        :param result_failed: Failed items.
        :param ret_list: List to store RetVals in.
        :return:
        """

        # Loop over failed items and handle errors
        for con, result in result_failed.items():
            # Append ret val
            ret_list.append(
                self.RetVal(
                    host=self._host_string_create(con),
                    status=self.RetValState.FAIL,
                    output=result)
            )

    def close_connections(self):
        """
        When using persistent connections, closes all open connections.
        """
        if self._connection_keep_open:
            self._t_group.close()

    def run_command(self, cmd):
        """
        Executes a command on the host.
        :param cmd: Command to be executed.
        :return: GroupResult.
        """

        try:
            self._logger.write_debug('Executing command: `{}`'.format(cmd))

            if self._connection_keep_open is False:
                with ThreadingGroup(*self._hosts,
                                    user=self._user,
                                    connect_kwargs={
                                        "timeout": self._connect_kwargs_timeout,
                                        "banner_timeout": self._connect_kwargs_banner_timeout,
                                        "auth_timeout": self._connect_kwargs_auth_timeout
                                    }) as t_group:

                    # Execute command and return result
                    g_result = t_group.run(cmd, hide=True)
            else:
                # Execute command and return result
                g_result = self._t_group.run(cmd, hide=True)

            self._logger.write_debug('Command result: `{}`'.format(g_result))
            return g_result

        except exceptions.GroupException as e:
            # GroupExceptions also contain results which must be returned
            self._logger.write_debug(e.args)
            return e.result
        except Exception as e:
            self._logger.write_exception(e.args)
            raise self.ExceptionRunCommand(e.args)

    def test_connection(self):
        """
        Tests a host connection by simply running a ls command and catching connections errors.
        :return:
        """

        # Execute command
        ret = self.run_command('ls')

        # List for return values
        ret_list = []

        # First handle `failed` items (need to be treated specially)
        self._result_handle_failed(ret.failed, ret_list)

        # Now loop over succeeded items
        for con in ret.succeeded.items():
            ret_list.append(
                self.RetVal(
                    host=self._host_string_create(con),
                    status=self.RetValState.OK,
                    output="connected")
            )

        return ret_list

    def uptime(self):
        """
        Returns uptime and load average.
        :return:
        """
        ret = self.run_command('echo "$(</proc/uptime)" "$(</proc/loadavg)"')

        # List for return values
        ret_list = []

        # First handle `failed` items (need to be treated specially)
        self._result_handle_failed(ret.failed, ret_list)

        # Now loop over `succeeded` items
        for con in ret.succeeded.items():
            # Append UptimeOutput object to ret list
            ret_list.append(
                self.RetVal(
                    host=self._host_string_create(con),
                    status=self.RetValState.OK,
                    output=self.UptimeOutput(con[1].stdout.strip())
                )
            )

        return ret_list

    def top_processes(self, command_names=[]):
        """
        Query top command.
        :param command_names: List with command names to be queried. Empty list for ALL.
        :return: List with an entry per host.
        """

        # Create the command. Run top in batch mode, 1 batch.
        cmd = 'top -b -n1'

        # Execute the command
        ret = self.run_command(cmd)

        # List for return values
        ret_list = []

        # First handle `failed` items (need to be treated specially)
        self._result_handle_failed(ret.failed, ret_list)

        # Now loop over `succeeded` items
        for con in ret.succeeded.items():
            # Create TopOutput objects list
            # Note: top output is `\n` separated and contains (doubled) spaces
            entry_output = [
                " ".join(ent.split()).split(" ")
                for ent in con[1].stdout.strip().split('\n')
            ]

            # Create objects
            # We join list elements [11:] to support spaces in command names
            top_outputs = [self.TopOutput(*x[0:11], " ".join(x[11:])) for x in entry_output[7:]]

            # Create ret list (filter command names)
            ret_list.append(
                self.RetVal(
                    host=self._host_string_create(con),
                    status=self.RetValState.OK,
                    output=[x for x in top_outputs if x.command in command_names]
                )
            )

        return ret_list

    def process_list(self, process_names=[], stat_fields=[]):
        """
        Query list of processes.
        :param process_names: List with process names to be queried. Empty list for ALL.
        :param stat_fields: List containing fields to query.
        :return: List with an entry per host.
        """

        # Create the command
        cmd = 'ps ax -o {},{} | grep -e "{}" | grep -v "grep"'.format(
            ",".join(stat_fields),
            self.ProcessListColumns.COMMAND.value,
            '" -e "'.join(process_names)
        )

        # Execute the command
        ret = self.run_command(cmd)

        # List for return values
        ret_list = []

        # First handle `failed` items (need to be treated specially)
        self._result_handle_failed(ret.failed, ret_list)

        # Now loop over `succeeded` items
        for con in ret.succeeded.items():
            # Create return list:
            # ps output is `\n` separated and contains (doubled) spaces
            entry_output = [
                " ".join(ent.split()).split(" ")
                for ent in con[1].stdout.strip().split('\n')
            ]

            # Format for output is {COMMAND:[OUTPUT_ARGS]}
            ret_list.append(
                self.RetVal(
                    host=self._host_string_create(con),
                    status=self.RetValState.OK,
                    output=[{out[-1]: out[:-1]} for out in entry_output]
                )
            )

        return ret_list
