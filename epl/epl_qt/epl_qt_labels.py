from PyQt5.QtWidgets import *
from PyQt5.Qt import Qt, QFont


class Label(QLabel):
    """
    Class providing PyQt label functionality.
    """

    def __init__(self,
                 text='',
                 alignment=Qt.AlignCenter,
                 font=QFont("Arial", 10, QFont.Bold),
                 frame_shape=QFrame.NoFrame,
                 frame_shadow=None,
                 line_width=0,
                 width=None,
                 height=None
                 ):
        """
        Init the label element.
        :param text: Label text.
        :param alignment: Qt.Align*.
        :param font: QFont*.
        :param frame_shape: QFrame*.
        :param frame_shadow: QFrame*.
        :param line_width: Line width for border.
        :param width: Element's fixed width.
        :param height: Element's fixed height.
        """
        # Init the base class
        super().__init__()

        self.setText(text)
        self.setAlignment(alignment)
        self.setFont(font)
        self.setFrameShape(frame_shape)
        self.setLineWidth(line_width)
        if frame_shadow is not None:
            self.setFrameShadow(frame_shadow)
        if width is not None:
            self.setFixedWidth(width)
        if height is not None:
            self.setFixedHeight(height)

    def set_text(self, text):
        """
        Sets the label text.
        :param text: Text string to be written.
        :return:
        """
        self.setText(text)
