from PyQt5.QtWidgets import QPushButton
from PyQt5.QtCore import QSize
import time, threading
import qtawesome as qta


class PushButton(QPushButton):
    """
    Class derived from QPushButton to extend its functionality.
    """

    def __init__(self, object_name='',
                 icon_standard_qta=None,
                 icon_active_qta=None,
                 icon_success_qta=None,
                 icon_error_qta=None,
                 icon_size_qta=None,
                 enabled=True,
                 clicked_slot=None,
                 max_timeout_reset=None,
                 timeout_error_functionality=True,
                 button_text=''):

        # Init the parent class
        super().__init__()

        self.setText(button_text)
        self.setObjectName(object_name)
        self.setEnabled(enabled)
        self._clicked_slot = clicked_slot
        self._timeout_error_functionality = timeout_error_functionality
        if self._clicked_slot is not None:
            self.clicked_signal_connect()

        # Set icons
        self._icon_standard = qta.icon(icon_standard_qta) if icon_standard_qta is not None else None
        self._icon_active = qta.icon(icon_active_qta, color='blue', animation=qta.Spin(self)) \
            if icon_active_qta is not None else None
        self._icon_success = qta.icon(icon_success_qta, color='green') if icon_success_qta is not None else None
        self._icon_error = qta.icon(icon_error_qta, color='red') if icon_error_qta is not None else None

        # Set icon
        if self._icon_standard is not None:
            self.set_icon_standard()

            # Set icon size
            if icon_size_qta is not None:
                self.setIconSize(QSize(icon_size_qta[0], icon_size_qta[1]))

        # Timer
        if max_timeout_reset is not None:
            self._max_timeout_reset = max_timeout_reset
            self._timer_max_timeout = None
        else:
            self._timer_max_timeout = None

    def set_icon_standard(self):
        """
        Sets the active button icon.
        :return:
        """
        if self._icon_standard is not None:
            self.setIcon(self._icon_standard)

    def set_icon_active(self):
        """
        Sets the active button icon.
        :return:
        """
        if self._icon_active is not None:
            self.setIcon(self._icon_active)

    def set_icon_success(self):
        """
        Sets the success button icon.
        :return:
        """
        if self._icon_success is not None:
            self.setIcon(self._icon_success)

    def set_icon_error(self):
        """
        Sets the error button icon.
        :return:
        """
        if self._icon_error is not None:
            self.setIcon(self._icon_error)

    def error_set_period(self, period=5):
        """
        Sets the error icon for a given period.
        :return:
        """
        self.set_icon_error()
        threading.Timer(period, self.set_icon_standard).start()

    def success_set_period(self, period=2):
        """
        Sets the success icon for a given period.
        :return:
        """
        self.set_icon_success()
        threading.Timer(period, self.set_icon_standard).start()

    def pending_set(self):
        """
        Disconnects the clicked signal and sets active icon.
        :return:
        """
        self.set_icon_active()
        self.clicked_signal_disconnect()

        # Reset the button
        if self._max_timeout_reset is not None:
            self._timer_max_timeout = threading.Timer(self._max_timeout_reset, self.timeout_callback)
            self._timer_max_timeout.start()

    def pending_reset(self):
        """
        Connects the clicked signal and sets standard icon.
        :return:
        """
        self.set_icon_standard()
        self.clicked_signal_connect()

        # Stop the reset timer
        if self._timer_max_timeout is not None:
            self._timer_max_timeout.cancel()

    def clicked_signal_connect(self):
        """
        Connects the clicked signal with the predefined (during init) slot.
        :return:
        """
        self.clicked.connect(self._clicked_slot)

    def clicked_signal_disconnect(self):
        """
        Disonnects the clicked signal from the predefined (during init) slot.
        :return:
        """
        self.clicked.disconnect(self._clicked_slot)

    def timeout_callback(self):
        """
        Sop the timer.
        :return:
        """
        self._timer_max_timeout.cancel()
        self.clicked_signal_connect()
        if self._timeout_error_functionality is True:
            self.error_set_period()
        else:
            self.success_set_period()


class LabelButton:

    def __init__(self):

        # simple_widget = qta.IconWidget('mdi.car-electric')
        # simple_widget.setCursor(QCursor(Qt.PointingHandCursor))
        # simple_widget.setIconSize(QSize(500,500))
        # simple_widget.mousePressEvent = mousePressEvent

        pass

