from PyQt5.QtWidgets import QGridLayout


# TODO commenting once finalized
class Grid(QGridLayout):

    def __init__(self):
        super(Grid, self).__init__()

        self.widgets = dict()

    def add_widget(self, widget, column, row):
        self.widgets[widget.objectName()] = widget

        self.addWidget(widget, column, row)

    def add_widget_auto(self, widget):
        row = int((self.count_widgets / 3) + 1)
        column = int(self.count_widgets - ((row-1) * 3))

        # Add to existing or new row?
        self.add_widget(widget, row, column)

    def remove_widget(self, widget):

        # Remove widget from internal dict
        del self.widgets[widget.objectName()]

        # Remove widget from layout
        widget.setParent(None)
        self.removeWidget(widget)
        widget.deleteLater()

        # Reinit the layout to bring the remaining widgets on the right positions
        self._reinit_layout()

    def _reinit_layout(self):

        # Remove all widgets from layout
        for i in range(0, self.count()):
            self.removeItem(self.itemAt(i))

        # Create a temp copy of the widgets dict, and clear it afterwards.
        # Thus we can use the add_widget_auto function for re-initialization
        temp_widgets = dict(self.widgets)
        self.widgets.clear()

        # Add widgets from the temp dict
        for _, widget in temp_widgets.items():
            self.add_widget_auto(widget)

    @property
    def count_widgets(self):
        return len(self.widgets)
