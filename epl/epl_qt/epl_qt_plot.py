from PyQt5.QtWidgets import QVBoxLayout, QHBoxLayout, QPushButton, QWidget
from enum import Enum, auto
import pyqtgraph as pg


##### Override class #####
class NonScientific(pg.AxisItem):
    def __init__(self, *args, **kwargs):
        super(NonScientific, self).__init__(*args, **kwargs)

    def tickStrings(self, values, scale, spacing):
        return [str(int(value * 1)) for value in values]  # This line returns the NonScientific notation value

    def logTickStrings(self, values, scale, spacing):
        return [str(int(value * 10)) for value in values]


class Plotter(QWidget):
    """
    Class providing plotting functionality.
    Derived from QWidget and pyqtgraph as graph plotter.
    """

    class GraphColors(Enum):
        """
        Enum to select graph colors.
        """
        AUTO = auto()
        RED = auto()
        GREEN = auto()
        BLUE = auto()
        CYAN = auto()
        MAGENTA = auto()
        YELLOW = auto()
        BLACK = auto()
        WHITE = auto()

    class GraphSymbols(Enum):
        """
        Enum to select graph symbols.
        """
        NONE = auto()
        CIRCLE = auto()
        SQUARE = auto()
        ADD = auto()

    # Mapping of GraphColors to actual color codes (abbreviations)
    _GraphSymbolsMapping = {
        GraphSymbols.CIRCLE.name: 'o',
        GraphSymbols.SQUARE.name: 's',
        GraphSymbols.ADD.name: '+'
    }

    _PenColorMapping = {
        GraphColors.RED.name: 'r',
        GraphColors.GREEN.name: 'g',
        GraphColors.BLUE.name: 'b',
        GraphColors.CYAN.name: 'c',
        GraphColors.MAGENTA.name: 'm',
        GraphColors.YELLOW.name: 'y',
        GraphColors.BLACK.name: 'k',
        GraphColors.WHITE.name: 'w',
    }

    def __init__(self,
                 show_legend=False,
                 plots=list(),
                 label_x='',
                 label_y='',
                 unit_x='',
                 unit_y='',
                 log_x=False,
                 log_y=False,
                 graph_title='',
                 background=GraphColors.AUTO,
                 pen_colors=list(),
                 symbols=list(),
                 show_grid=False,
                 nav_buttons=list()):
        """
        Init the plotter element.
        :param show_legend: Show graph's legend.
        :param plots: List with plot names to be added during init (more can be added later).
        :param label_x: x-axis label.
        :param label_y: y-axis label.
        :param unit_x: x-axis unit.
        :param unit_y: y-axis unit.
        :param graph_title: Graph's title.
        :param background: Graph's background color (choose from GraphColors).
        :param show_grid: Show x/y grid?
        :param nav_buttons: List with button to put on navigation toolbar. Events are btn_0..btn_n
        """

        # Init the base QWidget
        super().__init__()

        # Init variables
        self._graph_title = graph_title
        self.nav_buttons = dict()
        self.setObjectName(self._graph_title)

        # Internal layout is QVBox to have canvas and toolbar separated
        self.layout = QVBoxLayout()

        # Init the navigation bar
        self._navigation_bar_init(nav_buttons)

        # Create plot widget
        self.plt = pg.PlotWidget()

        # Graph title
        self.plt.setTitle(graph_title)

        # Set graph's background color
        if background == self.GraphColors.AUTO:
            self.plt.setBackground(background='default')
        else:
            self.plt.setBackground(background=self._PenColorMapping[background.name])

        # Show grid?
        self.plt.showGrid(x=show_grid, y=show_grid)

        # Set log mode
        self.plt.setLogMode(log_x, log_y)

        # Show legend?
        if show_legend is True:
            self.plt.addLegend()

        # Insert an empty line for every plot to initialize the plot_refs
        self.plot_refs = dict()

        # Check if number of color/signal configs match the plot number
        # Otherwise enable AUTO or DEFAULT settings
        if len(symbols) < len(plots):
            symbols = [self.GraphSymbols.CIRCLE for _ in plots]
        if len(pen_colors) < len(plots):
            pen_colors = [self.GraphColors.AUTO for _ in plots]

        # Add plot(s)
        for plot, symbol, pen_color in zip(plots, symbols, pen_colors):
            self.add_plot(label=plot, symbol=symbol, pen_color=pen_color)

        # Set axes labels/title
        self.plt.setLabel('bottom', label_x, units=unit_x)
        self.plt.setLabel('left', label_y, units=unit_y)

        # Add the plotting widget to layout
        self.layout.addWidget(self.plt)

        # Set layout
        self.setLayout(self.layout)

    def _navigation_bar_init(self, nav_buttons):
        """
        Init the navigation bar element.
        :param nav_buttons: List with navigation button names.
        :return:
        """

        # Create nav bar with optional elements
        nav_bar_layout = QHBoxLayout()

        for button_name in nav_buttons:
            self.nav_buttons[button_name] = QPushButton(button_name)

            # Button to remove widget (signal must be caught outside)
            self.nav_buttons[button_name].setObjectName(self._graph_title)

            # Display remove button on nav-bar
            nav_bar_layout.addWidget(self.nav_buttons[button_name])

        # Add nav-bar layout to main layout
        self.layout.addLayout(nav_bar_layout)

    def add_plot(self, label, symbol=GraphSymbols.CIRCLE, pen_color=GraphColors.AUTO):
        """
        Adds a plot to the graph.
        :param label: Name of plot.
        :param pen_color: Line color (choose from PenColors).
        :return:
        """

        # Insert an empty plot to get plot reference for further updates
        if label not in self.plot_refs:

            if pen_color == self.GraphColors.AUTO:
                # In AUTO mode, choose color from n colors automatically
                self.plot_refs[label] = self.plt.plot([1], [1], pen=(len(self.plot_refs), 10),
                                                      symbol=None if symbol is self.GraphSymbols.NONE else
                                                      self._GraphSymbolsMapping[symbol.name],
                                                      symbolPen=(len(self.plot_refs), 10),
                                                      symbolBrush=(len(self.plot_refs), 10),
                                                      name=label,
                                                      axisItems={'left': NonScientific(orientation='left')})
            else:
                # Use color parameters
                self.plot_refs[label] = self.plt.plot([1], [1], pen=self._PenColorMapping[pen_color.name],
                                                      symbol=None if symbol is self.GraphSymbols.NONE else
                                                      self._GraphSymbolsMapping[symbol.name],
                                                      symbolPen=self._PenColorMapping[pen_color.name],
                                                      symbolBrush=self._PenColorMapping[pen_color.name],
                                                      name=label,
                                                      axisItems={'left': NonScientific(orientation='left')})

    def plot(self, plot_name, data_y, data_x=None):
        """
        Plot x/y data
        :param data_x: List with x-data.
        :param data_y: List with y-data.
        :param plot_name: Name of plot to be updated.
        :return:
        """

        # Use plot ref to update the data for that line.
        if data_x is not None:
            self.plot_refs[plot_name].setData(data_x, data_y)
        else:
            self.plot_refs[plot_name].setData(data_y)
