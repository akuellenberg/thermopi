from PyQt5.QtWidgets import QMessageBox


class MessageBox(QMessageBox):
    """
    Class wrapping QMessageBox to make it more convenient.
    """

    def __init__(self, icon=QMessageBox.NoIcon, window_title='', text='', buttons=QMessageBox.Ok):
        """

        :param icon: QMessageBox icon.
        :param window_title: The window title.
        :param text: The window main text.
        :param buttons: QMessageBox buttons (can be ored)
        """

        # Init base class
        super().__init__()

        # Make settings
        self.setIcon(icon)
        self.setWindowTitle(window_title)
        self.setText(text)
        self.setStandardButtons(buttons)

        # Show message box
        self.exec_()
