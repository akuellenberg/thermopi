from PyQt5.QtCore import QThread, pyqtSignal
import time


class UpdateThread(QThread):
    """
    Class providing thread functionality:
    * Recurring signal is fired every n seconds.
    """

    update_signal = pyqtSignal()

    def __init__(self, period=1.0):
        """
        Inits the class and its base class.
        :param period: Period for the signal to be emitted (0 for only once).
        """
        # Init the base class
        QThread.__init__(self)

        # Init variables
        self._period = period
        self._stop_thread = False

    def __del__(self):
        self.wait()

    def run(self):
        """
        The main task.
        :return:
        """
        try:
            # If period is 0, emit the signal only once
            if self._period == 0:
                self.update_signal.emit()
                self._stop_thread = True

            # Start the thread
            while self._stop_thread is False:
                self.update_signal.emit()
                time.sleep(self._period)

        except Exception as e:
            print('exp:{}'.format(e))

    def stop_thread(self):
        """
        Stop the thread.
        :return:
        """
        self._stop_thread = True
        self.terminate()
