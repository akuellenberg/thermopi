from PyQt5.QtWidgets import (QTableWidget, QTableWidgetItem, QHeaderView)
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor, QFont
from epl.epl_qt.epl_qt_buttons import PushButton


class Table:
    """
    Class providing PyQt table functionality.
    """

    def __init__(self,
                 items_list=list(),
                 show_vertical_header=False,
                 show_horizontal_header=False,
                 horizontal_header_items=list(),
                 resize_modes_horizontal=list(),
                 resize_modes_vertical=list(),
                 ):
        """
        Initializes the table.
        :param items_list: List of lists (columns:rows) with table items.
        """

        # The table widget
        self.tableWidget = QTableWidget()

        # Header
        self.headerH = self.tableWidget.horizontalHeader()
        self.headerV = self.tableWidget.verticalHeader()
        self.tableWidget.verticalHeader().setVisible(show_vertical_header)
        self.tableWidget.horizontalHeader().setVisible(show_horizontal_header)

        # Setup column/row count
        self.tableWidget.setColumnCount(self.column_count_calculate(items_list))
        self.tableWidget.setRowCount(self.row_count_calculate(items_list))

        # Resize modes
        if len(resize_modes_horizontal) > 1:
            for i in range(0, self.tableWidget.columnCount()):
                self.headerH.setSectionResizeMode(i, resize_modes_horizontal[i])
        else:
            self.headerH.setSectionResizeMode(resize_modes_horizontal[0])

        if len(resize_modes_vertical) > 1:
            for i in range(0, self.tableWidget.columnCount()):
                self.headerV.setSectionResizeMode(i, resize_modes_vertical[i])
        else:
            self.headerV.setSectionResizeMode(resize_modes_vertical[0])

        # Add items to list
        self.add_items_from_list(items_list)

        # Set horizontal header labels
        self.header_hor_labels_set(horizontal_header_items)

        # TODO: Draggable/Movable currently not implemented
        # self.headerV.setSectionsMovable(False)
        # self.headerV.setDragEnabled(False)
        # self.headerV.setDragDropMode(QAbstractItemView.InternalMove)
        # self.tableWidget.doubleClicked.connect(self.on_click)

    def column_count_calculate(self, items_list):
        """
        Calculates number of columns based on the first entry in first sub table.
        :param items_list: List of sub table items.
        :return: Number of columns.
        """
        for key, value in items_list[0].items():
            return len(value[0]) - 1

    def row_count_calculate(self, items_list):
        """
        Iterates over the items list and count number of entries.
        Also counts the sub table headers.
        :param items_list: List of sub table items.
        :return: Number of rows including headers.
        """
        row_count = 0
        for sub_table in items_list:
            for key, value in sub_table.items():
                # +1 for the key holding the heading
                row_count += (len(value) + 1)

        return row_count

    def add_items_from_list(self, items_list):
        """
        Adds items from list to the table widget.
        :param items_list: List of lists (columns:rows) with table items.
        :return:
        """

        row = 0

        # Iterate through the sub tables
        for sub_table in items_list:

            for key, value in sub_table.items():

                # Insert sub table header
                self.tableWidget.setSpan(row, 0, 1, self.tableWidget.columnCount())
                self.tableWidget.setVerticalHeaderItem(row, QTableWidgetItem(''))
                self.cell_info_set(row, 0, key, Qt.ItemIsEnabled, alignment=Qt.AlignCenter, font=QFont('Arial', 10, QFont.Bold))
                self.set_row_color(row, QColor(153, 153, 255))
                self.headerV.setSectionResizeMode(row, QHeaderView.ResizeToContents)
                row += 1

                # Iterate through rows of sub table
                for row_item in value:
                    col = 0
                    # Iterate though cell-items
                    for i in range(0, len(row_item)):
                        cell_item = row_item[i]
                        if i == 0:
                            self.tableWidget.setVerticalHeaderItem(row, QTableWidgetItem(cell_item))
                            col -= 1
                        elif type(cell_item) is PushButton:
                            self.cell_widget_set(row, col, cell_item)
                        else:
                            self.cell_info_set(row, col, cell_item, Qt.ItemIsEnabled)
                        col += 1
                    row += 1

    def header_hor_labels_set(self, horizontal_labels):
        """
        Sets the horizontal header labels.
        :param horizontal_labels: List with horizontal header labels (empty list for int values).
        :return:
        """
        for i in range(0, len(horizontal_labels)):
            self.tableWidget.setHorizontalHeaderItem(i, QTableWidgetItem(horizontal_labels[i]))

    def header_value_get(self, row_column=0, horizontal_vertical=True):
        """
        Get a header label.
        :param row_column: Row/Column for header
        :param horizontal_vertical: True->horizontal header, False->vertical header.
        :return: Label-Test as string.
        """
        if horizontal_vertical is True:
            return self.tableWidget.horizontalHeaderItem(row_column).text()
        else:
            return self.tableWidget.verticalHeaderItem(row_column).text()

    def cell_info_set(self, row_index,
                      column_index,
                      cell_item,
                      item_flags=Qt.NoItemFlags,
                      alignment=(Qt.AlignLeft | Qt.AlignVCenter),
                      font=QFont('Arial')):
        """
        Sets the cell info on a specific cell.
        :param row_index: Row index of widget.
        :param column_index: Column index of widget.
        :param cell_item: The item to be inserted.
        :param item_flags: Item flags.
        :param alignment: Cell alignment.
        :param font: Cell-Font.
        :return:
        """
        cell_info = QTableWidgetItem(cell_item)
        cell_info.setFlags(item_flags)
        cell_info.setTextAlignment(alignment)
        cell_info.setFont(font)
        self.tableWidget.setItem(row_index, column_index, cell_info)

    def cell_info_get(self, row_index, column_index):
        """
        Returns the cell widget of a specific cell.
        :param row_index: Row index of widget.
        :param column_index: Column index of widget.
        :return: QTableWidgetItem
        """
        return self.tableWidget.item(row_index, column_index)

    def cell_widget_set(self, row_index, column_index, cell_widget):
        """
        Sets the cell widget on a specific cell.
        :param row_index: Row index of widget.
        :param column_index: Column index of widget.
        :param cell_widget: QTableWidget.cellWidget
        :return:
        """
        self.tableWidget.setCellWidget(row_index, column_index, cell_widget)

    def cell_widget_get(self, row_index, column_index):
        """
        Returns the cell widget of a specific cell.
        :param row_index: Row index of widget.
        :param column_index: Column index of widget.
        :return: QTableWidget.cellWidget
        """
        return self.tableWidget.cellWidget(row_index, column_index)

    def update_cell(self, row, column, update_value):
        """
        Updates a cell value.
        :param row: Row index.
        :param column: Column index.
        :param update_value: Value to be written.
        :return:
        """
        cell_info = QTableWidgetItem(update_value)
        cell_info.setFlags(self.tableWidget.item(row, column).flags())
        self.tableWidget.setItem(row, column, cell_info)

    def update_cell_color_cell(self, row, column, update_value, color=QColor(255, 255, 255)):
        """
        Updates a cell value and sets its background color.
        :param row: Row index.
        :param column: Column index.
        :param update_value: Value to be written.
        :param color: QColor
        :return:
        """
        self.update_cell(row, column, update_value)
        self.set_cell_color(row, column, color)

    def update_cell_color_row(self, row, column, update_value, color=QColor(255, 255, 255)):
        """
        Updates a cell value and sets the background color for entire row.
        :param row: Row index.
        :param column: Column index.
        :param update_value: Value to be written.
        :param color: QColor
        :return:
        """
        self.update_cell(row, column, update_value)
        self.set_row_color(row, color)

    def set_cell_color(self, row, column, color=QColor(255, 255, 255)):
        """
        Sets the background color of a single cell.
        :param row: Row index.
        :param column: Column index.
        :param color: QColor
        :return:
        """
        self.tableWidget.item(row, column).setBackground(color)

    def set_row_color(self, row_index, color=QColor()):
        """
        Sets the background color of the entire row.
        :param row_index: The row's index.
        :param color: QColor
        :return:
        """
        for i in range(self.tableWidget.columnCount()):
            item = self.tableWidget.item(row_index, i)
            # We have items and cell widgets.
            # Prevent widgets from being updated
            if item is not None:
                item.setBackground(color)

    def view_update(self):
        """
        Updates the table widget.
        :return:
        """
        self.tableWidget.viewport().update()
