from PyQt5.QtWidgets import *


class TabView(QTabWidget):
    """
    Class providing QTabWidget functionality.
    """
    def __init__(self, tab_names=list(), parent=None):
        """
        Inits the TabView object.
        :param tab_names: list with tab names.
        :param parent: See QTabWidget description.
        """
        super(TabView, self).__init__(parent)

        # Dict to store the tabs in
        self.tab_widgets = dict()

        # Create tabs based on tab names given
        i = 0
        for tab_name in tab_names:
            # Create a QWidget
            tab_wid = QWidget()

            # Add the QWidget to the container
            self.addTab(tab_wid, tab_name)

            # Set the tab text
            self.setTabText(i, tab_name)

            # Add tab to dict of tabs
            self.tab_widgets[tab_name] = tab_wid
            i += 1

    def tab_layout_set(self, tab_name, layout):
        """
        Sets the layout for a tab page.
        :param tab_name: Name of tab page.
        :param layout: Layout to be set.
        :return:
        """
        self.tab_widgets[tab_name].setLayout(layout)

    def event_current_changed(self, callback_changed):
        """
        Connects the current_changed event.
        :param callback_changed: Callback to connect to.
        :return:
        """
        self.currentChanged.connect(callback_changed)