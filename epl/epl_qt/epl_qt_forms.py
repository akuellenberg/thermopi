from PyQt5.QtWidgets import (QWidget, QHBoxLayout, QVBoxLayout)
from PyQt5.QtCore import pyqtSignal
from enum import Enum, auto


class Window(QWidget):
    """
    Class providing  application window functionality.
    """

    # Signals
    close_signal = pyqtSignal(int)
    resize_signal = pyqtSignal(int)
    change_signal = pyqtSignal(int)

    class BoxLayout(Enum):
        """
        Enum for box_layout parameter.
        """
        HORIZONTAL = auto()
        VERTICAL = auto()

    def __init__(self, title, left, top, width, height, box_layout=BoxLayout.HORIZONTAL):
        """
        Init the application window.
        :param title: Window title.
        :param left: Position left.
        :param top: Position top.
        :param width: Window width.
        :param height: Window height.
        :param box_layout: BoxLayout parameter to set layout for window widgets.
        """
        super().__init__()
        self.title = title
        self.left = left
        self.top = top
        self.width = width
        self.height = height
        self._widgets = dict()

        # Init the UI
        self._init_user_interface(box_layout)

    def _init_user_interface(self, box_layout=BoxLayout.HORIZONTAL):
        """
        Inits the window (sets title, height and width...)
        :return:
        """
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)

        if box_layout == self.BoxLayout.HORIZONTAL:
            self.layout = QHBoxLayout()
        elif box_layout == self.BoxLayout.VERTICAL:
            self.layout = QVBoxLayout()

        # Add box layout
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)

    def add_widget(self, widget):
        """
        Function to add widgets to the window.
        :param widget: Widget to be added.
        :return:
        """
        self._widgets[widget.objectName] = widget
        self.layout.addWidget(widget)
        self.setLayout(self.layout)

    def add_layout(self, layout):
        self.layout.addLayout(layout)

    def remove_widget(self, widget):
        """
        Function to remove a widget from the window.
        :param widget: Widget to be removed.
        :return:
        """
        del self._widgets[widget.objectName]
        self.layout.removeWidget(widget)
        self.setLayout(self.layout)

    def show_window(self):
        """
        Displays the window on screen.
        :return:
        """
        # Show window
        self.show()

    def hide_window(self):
        """
        Hides the window from screen.
        :return:
        """
        # Hide window
        self.hide()

    def bring_to_front(self):
        """
        Shows the window and brings it to top (only once, not topmost)
        :return:
        """
        self.show()
        self.activateWindow()

    def closeEvent(self, close_event):
        """
        Override of event on closing the widget.
        :param close_event: Info on event.
        :return:
        """
        self.close_signal.emit(1)

    def resizeEvent(self, resize_event):
        """
        Override of event on closing the widget.
        :param resize_event: Info on event.
        :return:
        """
        self.resize_signal.emit(1)

    def changeEvent(self, change_event):
        """
        Override of event on closing the widget.
        :param change_event: Info on event.
        :return:
        """
        self.change_signal.emit(1)
