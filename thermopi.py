from argparse import ArgumentParser
from epl.epl_logging.epl_logging_log import Log
from epl.epl_katcp.epl_katcp_gim import KatcpGimInfo
from epl.epl_datetime.epl_datetime_datetime import dt_now_tz_timestamp
from epl.epl_gim.epl_gim_exporter import GenericGimExporter
from gim_exporter.gim import GIM
import gim_exporter.gim
import asyncio
import signal
import socket

from gpiozero import MCP3208

# Stopper for the main thread
main_thread_stop = False

SENSOR_DEFINITIONS = {
    "temperatures.fokus":{
        "type": "mcp3208",
        "adc_ch":0,
        "limits":[10.0, 30]
    },
    "temperatures.maser":{
        "type": "mcp3208",
        "adc_ch":1,
        "limits":[18.0, 23]
    },
    "temperatures.apex":{
        "type": "mcp3208",
        "adc_ch":2,
        "limits":[10.0, 30]
    },
    "temperatures.koenigszapfenraum":{
        "type": "mcp3208",
        "adc_ch":3,
        "limits":[10.0, 25]
    },
    "temperatures.lofar_container":{
        "type": "barix",
        "barix_host": "134.104.75.2",
        "barix_port": 12302,
        "barix_sensor_id": 601,
        "limits":[10.0, 30]
    }
}



async def main():
    """
    The main routine.
    :return:
    """

    # Stopper for main thread
    global main_thread_stop

    async def init_app():
        # Initializes the args module
        args = argument_parser()

        # Initialize the loggers
        logger_main = Log(name='gge.main',
                          level_console=Log.LoggingLevels[args.log_level.upper()],
                          formatter_console=[Log.Formatters.ASC_TIME, Log.Formatters.FILE_NAME,
                                             Log.Formatters.FUNC_NAME, Log.Formatters.LINE_NUMBER],
                          file_name=args.log_file,
                          level_file=Log.LoggingLevels[args.log_level_file.upper()],
                          )
        logger_gim = Log(name='gge.gim',
                         level_console=Log.LoggingLevels[args.log_level.upper()],
                         formatter_console=[Log.Formatters.ASC_TIME, Log.Formatters.FILE_NAME,
                                            Log.Formatters.FUNC_NAME, Log.Formatters.LINE_NUMBER],
                         file_name=args.log_file,
                         level_file=Log.LoggingLevels.DEBUG,
                         )
        logger_exporter = Log(name='gge.exp',
                              level_console=Log.LoggingLevels[args.log_level.upper()],
                              formatter_console=[Log.Formatters.ASC_TIME, Log.Formatters.FILE_NAME,
                                                 Log.Formatters.FUNC_NAME, Log.Formatters.LINE_NUMBER],
                              file_name=args.log_file,
                              level_file=Log.LoggingLevels.DEBUG,
                              )
        logger_stats = Log(name='gge.app',
                           level_console=Log.LoggingLevels[args.log_level.upper()],
                           formatter_console=[Log.Formatters.ASC_TIME, Log.Formatters.FILE_NAME,
                                              Log.Formatters.FUNC_NAME, Log.Formatters.LINE_NUMBER],
                           file_name=args.log_file,
                           level_file=Log.LoggingLevels.DEBUG,
                           )

        # Create event loop
        loop = asyncio.get_event_loop()

        # Create gg_exporter
        logger_main.write_info("Creating gg_exporter")
        gg_exporter = GenericGimExporter(gim_url=args.gim_url, gim_user=args.gim_user, gim_pass=args.gim_pass,
                                         parent_device_name=args.gim_parent, policies=args.gim_policies, override_gim=True,
                                         logger=logger_exporter, sensor_min_update_rate_s=args.gim_sensor_min_update_rate)
        gim_exporter.gim.log = logger_gim  # Set gim logging
        await gg_exporter.init_connect()

        # Create heartbeat task
        loop.create_task(gg_exporter.heartbeat())

        # Connect exit handler to properly stop the application on ctrl+c
        for sig_name in ('SIGINT', 'SIGTERM'):
            loop.add_signal_handler(getattr(signal, sig_name),
                                    lambda sig_name=sig_name: asyncio.create_task(
                                        shutdown_application(sig_name)
                                    ))

        return logger_main, logger_stats, gg_exporter, loop, args.main_loop_wait

    async def shutdown_application(*kwargs):
        """
        Function to handle the application shutdown.
        Stop threads, disconnect clients...
        :return:
        """
        _logger_main.write_info('Preparing application shutdown...')

        # Stopper for main thread
        global main_thread_stop
        main_thread_stop = True

        # Stop gg_exporter
        await _gg_exporter.shutdown()

        _logger_main.write_info('Application will be shut down, be patient...')

    def handle_temp_task(task_tree_name, current_value, state, send_data, task_list):
        """
        Function to generically handle task updates.
        :param task_tree_name: Task's entire tree name (`.` separated hierarchy levels)
        :param current_value: Current value
        :param state: State of task [1:nominal, 2:warning, 3:error]
        :param send_data: Reference to data list to append send data
        :param task_list: List containing all tasks (needs to be stored outside)
        :return: bool: True->New entry, need to sync with gim; False->Data-Only.
        """

        sync_gim = False

        # New task -> add to list ?!
        if task_tree_name not in task_list:
            # Sensor's gim-config definition?
            task_list[task_tree_name] = KatcpGimInfo(
                description="Temperature task",
                options_available=KatcpGimInfo.OptionsAvailable.GET,
                mysql_task_type=KatcpGimInfo.WidgetType.GET,
                value_type="float",
                limits=[0, 0],
                unit="C",
                format_display="",
                icinga_check_constant_errors="0"
            )

            # Indicate need for a sync
            sync_gim = True

        # Add data
        send_data[task_tree_name] = [{
            "timestamp": "{}".format(dt_now_tz_timestamp()),
            "current_value": current_value,
            "value_type": "float",
            "error_state": state,
        }]

        return sync_gim


    # Declare variables and init
    _logger_main, _logger_stats, _gg_exporter, _loop, _main_loop_wait = await init_app()

    _task_list = {}
    _wait_counter = 0

    while main_thread_stop is False:
        try:
            # Wait counter to make the application stop faster
            if _wait_counter >= _main_loop_wait:
                _wait_counter = 0

                _logger_main.write_info('Next run...')

                # Send data list to collect all the data to be sent
                _send_data = {}
                _num_single_data = 0
                _sync_gim = False

                # Handle tasks
                for sensor_name, sensor_details in SENSOR_DEFINITIONS.items():
                    if sensor_details["type"] == "mcp3208":
                        raw = MCP3208(channel=sensor_details["adc_ch"], 
                                clock_pin=21, 
                                mosi_pin=20, 
                                miso_pin=19, 
                                select_pin=26, 
                                differential=False).raw_value
                        temp_C = float(raw) * 0.005 * 3.125 - 12.5
                        if temp_C > sensor_details["limits"][0] and temp_C < sensor_details["limits"][1]:
                            error_state = '1'
                        else:
                            error_state = '3'
                    elif sensor_details["type"] == "barix":
                        try:
                            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                            s.settimeout(10)
                            s.connect((sensor_details["barix_host"],sensor_details["barix_port"]))
                            command = "getio,{}\r".format(sensor_details["barix_sensor_id"])
                            s.sendall(command.encode())
                            data = s.recv(64)
                            x = data.decode().strip().split(',',3)
                            temp_C = float(x[2])*5.0/80.0
                            if temp_C > sensor_details["limits"][0] and temp_C < sensor_details["limits"][1]:
                                error_state = '1'
                            else:
                                error_state = '3'
                        except Exception as e:
                            _logger_main.write_error('Error connecting to Barix-Host ({}:{}): {}'.format(sensor_details["barix_host"],sensor_details["barix_port"], e))
                            error_state = '3'

                    _sync_gim |= handle_temp_task(
                        task_tree_name=sensor_name,
                        current_value=temp_C,
                        state=error_state,
                        send_data=_send_data,
                        task_list=_task_list)
                    _num_single_data += 1


                # Sync needed
                if _sync_gim:
                    _logger_main.write_info("Syncing gim tree...")
                    await _gg_exporter.update_from_tasklist(_task_list)

                # Execute the bulk request
                _logger_main.write_info('Performing sensor bulk update.'.format(_num_single_data))
                await _gg_exporter.sensor_bulk_update(_send_data)

                _logger_main.write_info('Run finished, sleeping {}s...'.format(_main_loop_wait))

        except Exception as e:
            _logger_main.write_exception('Some error: {}'.format(e))
        finally:
            _wait_counter += 1
            await asyncio.sleep(1)

    _logger_main.write_info('Done closing connections to remote hosts.')
    _logger_main.write_info('Main loop stopped, bye bye.')


def argument_parser():
    """
    Set up the ArgumentParser module.
    :return: Parsed args as object.
    """

    arg_pars = ArgumentParser()
    arg_pars.add_argument('-l', '--log-level', required=False,
                          choices=[level.name.lower() for level in Log.LoggingLevels], dest='log_level',
                          default=Log.LoggingLevels.INFO.name.lower(),
                          help='Set one of the available logging levels {}.'.format(
                              [level.name.lower() for level in Log.LoggingLevels]))
    arg_pars.add_argument('--log-level-file', required=False,
                          choices=[level.name.lower() for level in Log.LoggingLevels], dest='log_level_file',
                          default=Log.LoggingLevels.INFO.name.lower(),
                          help='Set one of the available logging levels for file logging {}.'.format(
                              [level.name.lower() for level in Log.LoggingLevels]))
    arg_pars.add_argument('--log-file', action='store', dest='log_file',
                          type=str, help='Log-File to log to.',
                          default='')
    arg_pars.add_argument('--gim-url', action='store', dest='gim_url',
                          type=str, help='The GIM URL address to connect to',
                          default="http://134.104.74.59:30680")
    arg_pars.add_argument('--gim-user', action='store', dest='gim_user',
                          type=str, help='The GIM username', default="sidecar")
    arg_pars.add_argument('--gim-pass', action='store', dest='gim_pass',
                          type=str, help='The GIM password', default="ysabtf4qzG6fNcd")
    arg_pars.add_argument('--gim-parent', action='store', dest='gim_parent',
                          required=True, type=str,
                          help='The parent device in the GIM tree to search/put this device under')
    arg_pars.add_argument('--gim-policy', action='append', type=str, dest='gim_policies',
                          help='specify a policy to be used when encountering unknown sensors or servers without a '
                               '?gim-config call.',
                          default=None)
    arg_pars.add_argument('--gim-sensor-min-update-rate', action='store', type=int, dest='gim_sensor_min_update_rate',
                          help='Minimum rate (in s) a sensor will updated with', required=False,
                          default=1800)
    arg_pars.add_argument('--main-loop-wait', action='store', type=int, dest='main_loop_wait',
                          help='Wait time (in s) between two main loop runs.', required=False,
                          default=30)

    return arg_pars.parse_args()


if __name__ == "__main__":
    try:
        _loop_main = asyncio.get_event_loop()
        _loop_main.run_until_complete(main())
    finally:
        _loop_main.stop()
